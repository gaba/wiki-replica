# Autistici / Inventati

A/I hosts:

- the boum.org DNS (still used by Tails, eg. gitlab.tails.boum.org)
- the boum.org MX servers
- Tails' Mailman mailing lists

## Contact

- E-mail: info@autistici.org
- IRC: #ai on irc.autistici.org
