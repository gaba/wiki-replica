# PUSCII

PUSCII hosts:

- `teels.tails.net`, a VM for Tails secondary dns
- several of Tails' Schleuder lists

## Contact

- E-mail: admin@puscii.nl
- IRC: `#puscii` on irc.indymedia.org
