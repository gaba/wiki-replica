# Providers

This page points to doc for the infrastructure and service providers we use.
Note that part of the documentation (eg. emergency contacts and info for oob
access) lives in the [password-manager](service/password-manager).

| provider      | service/infra                        | system-specific doc                |
| ------------- | ------------------------------------ | ---------------------------------- |
| [Autistici][] | email and DNS for Tails              |                                    |
| Coloclue      | colocation for Tails                 | [chameleon][], [stone][]           |
| Hetzner       | `gnt-fsn` cluster nodes              | [Cloud][], [Robot][]               |
| [Paulla][]    | dev server for Tails                 |                                    |
| [Puscii][]    | virtual machines and email for Tails | [teels][]                          |
| SEACCP        | physical machines for Tails          | [dragon][], [iguana][], [lizard][] |
| [Quintex][]   | `gnt-dal` cluster nodes              |                                    |
| Tachanka!     | virtual machines for Tails           | [ecours][], [gecko][]              |

[Autistici]: provider/autistici
[Cloud]: howto/new-machine-hetzner-cloud
[chameleon]: tails/systems/chameleon/notes
[dragon]: tails/systems/dragon/notes
[ecours]: tails/systems/ecours/notes
[gecko]: tails/systems/gecko/notes
[iguana]: tails/systems/iguana/notes
[lizard]: tails/systems/lizard/notes
[Robot]: howto/new-machine-hetzner-robot
[Paulla]: tails/systems/skink/notes
[Puscii]: provider/puscii
[Quintex]: provider/quintex
[stone]: tails/systems/stone/notes
[teels]: tails/systems/teels/notes
