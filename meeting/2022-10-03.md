# Roll call: who's there and emergencies

anarcat, kez, lavamind, no emergencies.

# Dashboard review

We did our normal per-user check-in:

 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=anarcat
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=kez
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=lavamind

And reviewed the general dashboards:

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

# Estimates workshop

We worked on what essentially became [TPA-RFC-40][]. Some notes were
taken in a private issue, but most of the work should be visible in
the above.

[TPA-RFC-40]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-40-cymru-migration

# Next meeting

We should look at OKRs in November to see if we use them for 2023. A
bunch of TPA-RFC (especially TPA-RFC-33) should be discussed
eventually as well. Possibly make another meeting next week.

# Metrics of the month

 * hosts in Puppet: 98, LDAP: 98, Prometheus exporters: 168
 * number of Apache servers monitored: 31, hits per second: 704
 * number of self-hosted nameservers: 6, mail servers: 10
 * pending upgrades: 59, reboots: 1
 * average load: 1.21, memory available: 4.55 TiB/5.88 TiB, running processes: 737
 * disk free/total: 35.19 TiB/93.28 TiB
 * bytes sent: 405.23 MB/s, received: 264.06 MB/s
 * planned bullseye upgrades completion date: 2022-10-15
 * [GitLab tickets][]: 186 tickets including...
   * open: 0
   * icebox: 144
   * backlog: 23
   * next: 8
   * doing: 4
   * needs information: 6
   * needs review: 1
   * (closed: 2882)

 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bullseye/

Now also available as the main Grafana dashboard. Head to
<https://grafana.torproject.org/>, change the time period to 30 days,
and wait a while for results to render.
