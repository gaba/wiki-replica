#!/bin/sh

set -e
set -u

WIKI_DIR=~/wikis/help.torproject.org

podman run -it --rm \
       -v $PWD:/docs \
       -v $WIKI_DIR/.vale:/styles \
       -v $WIKI_DIR/.vale.ini:/docs/.vale.ini \
       -w /docs \
       docker.io/jdkato/vale "${1:-.}"
