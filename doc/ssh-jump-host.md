---
title: learning how to do an ssh jump host on tpo
---

You need to use an ssh jump host to access internal machines at tpo.
If you have a recent enough ssh (>= 2016 or so), then you can use the `ProxyJump` directive.  Else, use `ProxyCommand`.
`ProxyCommand` automatically executes the ssh command on the host to jump to the next host and forward all traffic through.

With recent ssh versions:

    Host *.torproject.org !ssh.torproject.org !people.torproject.org !gitlab.torproject.org
      ProxyJump ssh.torproject.org

Or with old ssh versions (before OpenSSH 7.3, or Debian 10 "buster"):

    Host *.torproject.org !ssh.torproject.org !people.torproject.org !gitlab.torproject.org
      ProxyCommand ssh -l %r -W %h:%p ssh.torproject.org

Note that there are multiple `ssh`-like aliases that you can use,
depending on your location (or the location of the target host). Right
now there are two:

 * `ssh-dal.torproject.org` - in [Dallas, TX, USA](https://en.wikipedia.org/wiki/Dallas)
 * `ssh-fsn.torproject.org` - in [Falkenstein, Saxony, Germany](https://en.wikipedia.org/wiki/Falkenstein,_Saxony)

The canonical list for this is searching for `ssh` in the `purpose`
field on the [machines database](https://db.torproject.org/machines.cgi). 

> Note: It is perfectly acceptable to run `ping` against the server to
> determine the closest to your location, and you can also run ping
> *from* the server *to* a target server as well. The shortest path
> will be the one that has the lowest *sum* for those two, naturally.

This naming convention was announced in [TPA-RFC-59](policy/tpa-rfc-59-ssh-jump-host-aliases).

# Host authentication

It is also worth keeping the `known_hosts` file in sync to avoid
server authentication warnings. The server's public keys are also
available in DNS. So add this to your `.ssh/config`:

    Host *.torproject.org
      UserKnownHostsFile ~/.ssh/known_hosts.torproject.org
      VerifyHostKeyDNS ask

And keep the `~/.ssh/known_hosts.torproject.org` file up to date by
regularly pulling it from a TPO host, so that new hosts are
automatically added, for example:

    rsync -ctvLP ssh.torproject.org:/etc/ssh/ssh_known_hosts ~/.ssh/known_hosts.torproject.org

Note: if you would prefer the above file to not contain the shorthand hostname
notation (i.e. `alberti` for `alberti.torproject.org`), you can get rid of those
with the following command after the file is on your computer:

    sed -i 's/,[^,.: ]\+\([, ]\)/\1/g' .ssh/known_hosts.torproject.org

# Different usernames

If your local username is different from your TPO username, also set
it in your `.ssh/config`:

    Host *.torproject.org
      User USERNAME

# Root access

Members of TPA might have a different configuration to login as root
by default, but keep their normal user for key services:

    # interact as a normal user with Puppet, LDAP, jump and gitlab servers by default
    Host puppet.torproject.org db.torproject.org ssh.people.torproject.org people.torproject.org gitlab.torproject.org
      User USERNAME

    Host *.torproject.org
      User root

Note that git hosts are not strictly necessary as you should normally
specify a `git@` user in your git remotes, but it's a good practice
nevertheless to catch those scenarios where that might have been
forgotten.

# When not to use the jump host

If you're going to do a *lot* of batch operations on all hosts (for
example with [Cumin](howto/cumin)), you definitely want to add yourself to the
[adding yourself to the allow list](howto/puppet#adding-an-ip-address-to-the-global-allow-list) so that you can *skip* using
the jump host.

For this, anarcat uses a special `trusted-network` command that fails
unless the network is on that allow list. Therefore, the above jump
host exception list becomes:

    # use jump host if the network is not in the trusted whitelist
    Match host *.torproject.org, !host ssh.torproject.org, !host ssh-dal.torproject.org, !host ssh-fsn.torproject.org, !host people.torproject.org, !host gitlab.torproject.org, !exec trusted-network
      ProxyJump anarcat@ssh-dal.torproject.org

The [`trusted-network` command](https://gitlab.com/anarcat/scripts/-/blob/3f3f024b9b2cbd0a32687727be7b26c8594ef933/trusted-network) checks for the default gateway on
the local machine and checks if it matches an allow list. It could
also just poke at the internet to see "what is my IP address", like:

- <https://check.torproject.org/>
- <https://wtfismyip.com/text>
- <https://ifconfig.me/ip>
- <https://ip.me/>
- <https://test.anarc.at/>

# Sample configuration

Here is a redacted copy of anarcat's `~/.ssh/config` file:

```
Host *
     # disable known_hosts hashing. it provides little security and
     # raises the maintenance cost significantly because the file
     # becomes inscrutable
     HashKnownHosts no
     # this defaults to yes in Debian
     GSSAPIAuthentication no
     # set a path for the multiplexing stuff, but do not enable it by
     # default. this is so we can more easily control the socket later,
     # for processes that *do* use it, for example git-annex uses this.
     ControlPath ~/.ssh/control-%h-%p-%r
     ControlMaster no
     # ~C was disabled in newer OpenSSH to facilitate sandboxing, bypass
     EnableEscapeCommandline yes

# taken from https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO/ssh
Host *-tor *.onion
    # this is with netcat-openbsd
    ProxyCommand nc -x 127.0.0.1:9050 -X 5 %h %p
    # if anonymity is important (as opposed to just restrictions bypass), you also want this:
    # VerifyHostKeyDNS no

# interact as a normal user with certain symbolic names for services (e.g. gitlab for push, people, irc bouncer, etc)
Host db.torproject.org git.torproject.org git-rw.torproject.org gitlab.torproject.org ircbouncer.torproject.org people.torproject.org puppet.torproject.org ssh.torproject.org ssh-dal.torproject.org ssh-fsn.torproject.org
  User anarcat

# forward puppetdb for cumin by default
Host puppetdb-01.torproject.org
  LocalForward 8080 127.0.0.1:8080

Host minio*.torproject.org
  LocalForward 9090 127.0.0.1:9090

Host prometheus2.torproject.org
  # Prometheus
  LocalForward 9090 localhost:9090
  # Prometheus Pushgateway
  LocalForward 9091 localhost:9091
  # Prometheus Alertmanager
  LocalForward 9093 localhost:9093
  # Node exporter is 9100, but likely running locally
  # Prometheus blackbox exporter
  LocalForward 9115 localhost:9115

Host dal-rescue-02.torproject.org
  Port 4622

Host *.torproject.org
  UserKnownHostsFile ~/.ssh/known_hosts.d/torproject.org
  VerifyHostKeyDNS ask
  User root

# use jump host if the network is not in the trusted whitelist
Match host *.torproject.org, !host ssh.torproject.org, !host ssh-dal.torproject.org, !host ssh-fsn.torproject.org, !host people.torproject.org, !host gitlab.torproject.org, !exec trusted-network
  ProxyJump anarcat@ssh-dal.torproject.org
```
