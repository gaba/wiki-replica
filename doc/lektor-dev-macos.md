---
title: Lektor website development environment on macOS
---

# Overview

The aim of this document is to explain the steps required to set up a local
Lektor development environment suitable for working on Tor Project websites
based on the Lektor platform.

We'll be using the [Sourcetree][] git GUI to provide a user-friendly method of
working with the various website's git repositories.

[Sourcetree]: https://www.sourcetreeapp.com/

# Prerequisites

First we'll install a few prerequisite packages, including Sourcetree.

You must have administrator privileges to install these software packages.

First we'll install the Xcode package.

Open the Terminal app and enter:

    xcode-select --install

Click `Install` on the dialog that appears.

Now, we'll install the `brew` package manager, again via the Terminal:

    /bin/bash -c "$(curl -fsSL
    https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

Now we're ready to install a few more tools:

    brew install coreutils git git-lfs python3.8

And lastly we need to download and install Sourcetree. This can be done from
the app's website: https://www.sourcetreeapp.com/

Follow the installer prompts, entering name and email address so that the git
commits are created with adequate identifying information.

![](lektor-dev-macos/install_preferences.png)

# Connect GitLab account

*This step is only required if you want to create Merge Requests in GitLab.*

Next, we'll create a GitLab token to allow Sourcetree to retrieve and update
projects.

 * Navigate to https://gitlab.torproject.org/-/profile/personal_access_tokens
 * Enter `sourcetree` under **Token name**
 * Choose an expiration date, ideally not more than a few months
 * Check the box next to `api`
 * Click **Create personal access token**
 * Copy the token into your clipboard

Now, open Sourcetree and click the **Connect...** button on the main windows,
then **Add...**, and fill in the dialog as below. Paste the token in the
**Password** field.

![](lektor-dev-macos/add_account.png)

Click the **Save** button.

The **Remote** tab on the main window should now show a list of git
repositories available on the Tor Project GitLab.

To clone a project, enter its name (eg. `tpo` or `blog`) in the **Filter
repositories** input box and click the **Clone** link next to it.

Depending on the project, a dialog titled **Git LFS: install required** may
then appear. If so, click **Yes** to ensure all the files in the project are
downloaded from GitLab.
