---
title: TPA-RFC-1: RFC process
deadline: 2020-02-14
status: standard
---

Summary: policy decisions should be made in an online consensus
building process with a 2 days to 2 weeks delay, and formally
documented in this wiki.

# Background

In the sysadmin team (AKA "TPA"), decisions can be made by individuals
in their daily work, in the regular online or in-person meetings, or
through an asynchronous online decision making process. This proposal
documents the latter decision making process and also serves as an
example of such proposal.

The idea behind this process is to include people for major changes so
that we don't get into a "hey wait we did what?" situation later. It
also allows decisions to be moved outside of meetings to have a faster
decision making process.

We already have the possibility of doing such changes right now, but
it's unclear how that process works or if it works at all. This is
therefore a formalization of this process.

We do understand that people can make mistakes and might improvise
sometimes, especially if process is not currently documented.

# Proposal

## Scope

This procedure aims to provide process for complex questions that:

 * might impact more than one system
 * define a contract between clients or other team members
 * add or replace tools or languages to the stack
 * build or rewrite something from scratch

When in doubt, use the process.

It is not designed for day-to-day judgement calls and regular
operations that do not fundamentally change our work processes. 

It also does not cover the larger [Tor Project policies][] as a
whole. When there is a conflict between the policies defined here and
the larger Tor policies, the latter policies overrule.

[Tor Project policies]: https://gitlab.torproject.org/tpo/community/policies/

## Communication

Decisions in the above scope should be written as a formal proposal,
explaining the purpose and a formal deadline, along with any relevant
background information. Such proposals are brought up to seek feedback
from peers in good faith, and assume trust between team members.

Proposals should be written in a Markdown document in a wiki with
revision history (currently this wiki).

A notification of the proposal must also be sent by email to the team
alias (currently `tpa-team@lists.torproject.org`). If the proposal
affects other teams outside of TPA, it should also be created as a
"ticket" in the ticket tracking software (currently "GitLab") so that
other teams can provide feedback.

Each proposal has a unique identifier made up of the string `TPA-RFC-`
and a unique, incremental number. This proposal, for example, is
`TPA-RFC-1` and the next one would be `TPA-RFC-2`. 

## Process

When the proposal is first written, the proposal is considered a
`draft`. When a notification is sent, the proposal is in the
`proposed` state and then enters a discussion period during which
changes can be proposed and objections can be raised. That period
ranges from 2 business days and two weeks and is picked in good faith
by the proposer based on the urgency of the changes proposed.

Objections must be formulated constructively and justified with
reasonable technical or social explanations. The goal of this step is
to communicate potential negative impacts and evaluate if they
outweigh the possible benefits of the proposal.

If the negative impacts outweigh the benefits, a constructive
objection must also propose changes that can be made to the proposal to
mitigate those problems.

## States

A proposal is in any of the following states:

 1. `draft`
 2. `proposed`
 2. `standard`
 3. `rejected`
 4. `obsolete`

Here is a graph of the possible state transitions:

![workflow.png](tpa-rfc-1-policy/workflow.png)

Once the discussion period has passed and no objection is raised, the
`proposed` RFC is adopted and becomes a `standard`.

If objections are raised and no solution is found, the proposal is
`rejected`.

Some policies can be completely overridden using the current policy
process, including this policy, in which case the old policy becomes
`obsolete`. Old, one-time decisions can also be marked as `obsolete`
when it's clear they do not need to be listed in the main policy
standards.

A policy can also be **modified** (instead of **overridden** by later
proposals or decisions taken in meetings, in which case it stays a
`standard`.

For TPA-RFC process changes, the older policy is modified only when
the new one becomes `standard`. For example, say `TPA-RFC-X` proposes
changes to a previous `TPA-RFC-N` proposal. In that case, the text of
`TPA-RFC-N` would be modified when and only if `TPA-RFC-X` is adopted
as a `standard`. The older `TPA-RFC-N` would also stay a `standard`,
although the *newer* `TPA-RFC-X` would actually become `obsolete` as
soon as the older `TPA-RFC-N` is modified.

# Examples

Examples of ideas relevant for the RFC process:

 * replacing Munin with Grafana and prometheus [#29681][]
 * setting default locale to C.UTF-8 [#33042][]
 * using Ganeti as a clustering solution
 * using setup-storage as a disk formatting system
 * setting up a loghost
 * switching from syslog-ng to rsyslog
 * changes to the RFC process

[#33042]: https://bugs.torproject.org/33042
[#29681]: https://bugs.torproject.org/29681

Counter examples:

 * setting up a new Ganeti node (part of the roadmap)
 * performing security updates (routine)
 * picking a different hardware configuration for the new Ganeti node
   (process wasn't documented explicitly, we accept honest mistakes)

Examples of obsolete proposals:

 * [TPA-RFC-4: prometheus disk](tpa-rfc-4-prometheus-disk) was marked as obsolete a while
   after the change was implemented.

# Deadline

Considering that the proposal was discussed and informally approved at
the [February 2020 team meeting][], this proposal will be adopted
within one week unless an objection is raised, which is on 2020-02-14
20:00UTC.

[February 2020 team meeting]: https://gitlab.torproject.org/anarcat/wikitest/-/wikis/meeting/2020-02-03/

# References

This proposal is one of the takeaways anarcat got from reading the
[guide to distributed teams][] was the idea of using [technical RFCs
as a management tool][].

 [guide to distributed teams]: https://increment.com/teams/a-guide-to-distributed-teams/
 [technical RFCs as a management tool]: https://buriti.ca/6-lessons-i-learned-while-implementing-technical-rfcs-as-a-management-tool-34687dbf46cb

This process is similar to the [Network Team Meta Policy][] except it
doesn't require a majority "+1" votes to go ahead. In other words,
silence is consent.

[Network Team Meta Policy]: https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/MetaPolicy

This process is also similar to the [RFC process discussed here](https://candost.blog/how-to-stop-endless-discussions/)
which also introduces the idea of "the NABC model from Stanford \[which
defines\] the Need, followed by Approach, Benefits, and lastly,
Competitors" and could eventually be added to this policy.
