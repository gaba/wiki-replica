---
title: TPA-RFC-64: Puppet TLS certificates
costs: None, weasel is volunteering.
approval: @anarcat verbally approved at the Lisbon meeting
affected users: TPA
status: standard
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41610
---

[[_TOC_]]

# Proposal

Move from `letsencrypt-domains.git` to Puppet to manage TLS
certificates. 

## Migration Plan

### Phase I

add a new boolean param to `ssl::service` named "`dehydrated`".

If set to `true`, it will cause `ssl::service` to create a key
and request a cert via Puppet dehydrated.

It will not install the key or cert in any place we previously used,
but the new key will be added to the TLSA set in DNS.

This will enable us to test cert issuance somewhat.

### Phase II

For instances where `ssl::service` `dehydrated` param is true
and we have a cert, we will use the new key and cert and install
it in the place that previously got the data from puppet/LE.

### Phase III

Keep setting `dehydrated` to true for more things.  Once all are true,
retire all `letsencrypted-domains.git` certs.

### Phase IV

profit

### Phase XCIX

Long term, we may retire `ssl::service` and just use `dehydrated::certificate`
directly.  Or not, as `ssl::service` also does TLSA and onion stuff.
