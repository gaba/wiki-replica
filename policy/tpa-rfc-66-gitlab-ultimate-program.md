---
title: TPA-RFC-66: Migrate to Gitlab Ultimate Edition
costs: Non 
approval: tor-internal
affected users: Tor community
deadline: December 15th 2024
status: draft
discussion: https://gitlab.torproject.org/tpo/team/-/issues/202
---

[[_TOC_]]

Summary: switch, by March 2025, Gitlab from the Community Edition license to
Enterprise edition license, to improve project management at Tor.

# Background

In June 2020, we migrated from the bug tracking system Trac to Gitlab.
At that time we considered to use Gitlab Enterprise but the decision
of moving to Gitlab was a big one already and we decided to go one
step at a time.

As a reminder, we migrated from Trac to GitLab because:

* GitLab allowed us to consolidate engineering tools into a single
  application: Git repository handling, wiki, issue tracking, code
  reviews, and project management tooling.

* GitLab is well-maintained, while Trac was not as actively
  maintained; Trac itself hadn't seen a release for over a year (in
  2020; there has been a stable release in 2021 and 2023 since).

* GitLab enabled us to build a more modern CI platform.

So moving to Gitlab was a good decision and we have been improving how
we work in projects and maintain the tools we developed. It has been
good for tackling old tickets, requests and bugs.

Still, there are limitations that we hope we can overcome with the
features in the premium tier of Gitlab. This document explains how we
are working on projects as well as trying to understand which new features Gitlab Ultimate has and how we can use them. Not all the features listed in this document will be used but it will be up to project managers and teams to agree on how to use the features available. 

It assumes familiarity of the [project life cycle][] at Tor.

# Proposal

We will switch from Gitlab Community Edition to use [Gitlab
Ultimate][], still as a self-managed deployment but with a [non-free
license][]. We'd use a free (as in "money") option GitLab offers for
non-profit and open source projects.

[Gitlab Ultimate]: https://about.gitlab.com/pricing/ultimate/
[non-free license]: https://about.gitlab.com/pricing/licensing-faq/

## Goals

To improve how we track activities and projects from the beginning to
the end. 

## Features comparison

This section reviews the features from Gitlab Ultimate in comparison
with Gitlab Community Edition.

### Multiple Reviewers in Code Reviews

**Definition**: It is the activity we do with all code that will be
merged into the tools that Tor Project maintains. For each merge
request we have at least one person reading through all the changes in
the code. 

In Gitlab Ultimate, we will have

* [multiple-reviewers merge requests][]
* [push-rules][] that enable more control over what can and can’t be
  pushed to your repository through a user-friendly interface

**How we are using them now**: We have a ‘triage bot’ in some of the
projects that assigns a code reviewer once a merge request is ready to
be reviewed. 

The free edition only allows a single reviewer to be assigned a merge
request, and only GitLab administrators can manage server-side hooks.

[multiple-reviewers merge requests]: https://docs.gitlab.com/ee/user/project/merge_requests/reviews/
[push-rules]: https://docs.gitlab.com/ee/user/project/repository/push_rules.html

### Custom Permissions

**Definition**: In Gitlab we have roles with different permissions. When the user is added to the project or group they need to have a specific role assigned. The role defines which actions they can take in that Gitlab project or group.

Right now we have the following roles:

- guest
- reporter
- developer
- maintainer
- owner

In Gitlab Ultimate, we could create [custom roles][] to give specific
permissions to users that are different from the default roles. 

We do not have a specific use case for this feature at Tor right now.

**How we are using them now**: In the top level group “tpo” we have
people (e.g. @anarcat-admin, @micah and @gaba) with the owner role and
others (e.g. @gus, @isabela and @arma) with reporter role. Then each
sub-group has the people of their team and collaborators.

[custom roles]: https://docs.gitlab.com/ee/user/permissions.html#custom-roles

### Epics 

**Definition**: Gitlab Ultimate offers ‘[epics][]’ to group together
issues across projects and milestones. You can assign labels, and a
start/end date to the epic as well as to have child epics. In that way
it creates a visual, tree-like, representation of the road map for
that epic.

**How we are using them now**: Epics do not exist in Gitlab Community Edition.

**What problem we are solving**: It will bring a representation of the
roadmap *into GitLab*. Right now we have the ‘all teams planning’
spreadsheet (updated manually) in NextCloud that shows the roadmap per
team and the assignments. 

(We used to do this only with pads and wiki pages before.)

We may still need to have an overview (possible in a spreadsheet) of the roadmap with allocations to be able to understand capacity of each team.

Epics can be used for roadmapping a specific projects. An epic is a
“bucket of issues” for a specific deliverable. We will not have an
epic open ‘forever’ but it will be done when all the issues are done
and the objective for the epic is accomplished. Epics and issues can
have Labels. In that case we use the labels to mark the project
number.

For example we can use one epic with multiple child-epics to roadmap
the work that needs to be done to complete the development of Arti
relays and the transition of the network. We will have issues for all
the different tasks that need to happen in the project and all of them
will be part of the different epics in the ‘Arti relays’ project. The
milestones will be used for planning specific releases.

#### Difference between Epics and Milestones

Milestones are better suited for planning release timelines and tracking specific features, allowing teams to focus on deadlines and delivery goals.

Epics, on the other hand, are ideal for grouping related issues across multiple milestones, enabling high-level planning and tracking for larger project goals or themes.

Milestones are timeline-focused, while epics organize broader, feature-related goals. 

For example, we could have a milestone to track the connect assist implementation in Tor Browser for Android until we are ready to include it in a release.

[epics]: https://docs.gitlab.com/ee/user/group/epics/

### Burndown and Burnup charts for milestones

**Definition**: In Gitlab, [milestones][] are a way to track issues and
merge requests to achieve something over a specific amount of time.

In Gitlab Ultimate, we will have [burndown and burnup charts][]. A
burndown chart visualizes the number of issues remaining over the
course of a milestone. A burnup chart visualizes the assigned and
completed work for a milestone.

**How we are using them now**: When we moved from Trac into Gitlab we
started using milestones to track some projects. Then we realized that
it was not working so well as we may also need to use milestones for specific releases. Now we are using milestones to track releases as well for tracking specific features or goals on a project.

**What problem we are solving**: We will be able to understand better
the progress of a specific milestone. GitLab Ultimate's burndown and burnup charts enable better milestone tracking by offering real-time insights into team progress. These tools help to identify potential bottlenecks, measure progress accurately, and support timely adjustments to stay aligned with project goals. Without such visual tools, it’s challenging to see completion rates or the impact of scope changes, which can delay deliverables. By using these charts, teams can maintain momentum, adjust resource allocation effectively, and ensure alignment with the project's overall timeline.

Burndown charts help track progress toward milestone completion by showing work remaining versus time, making it easy to see if the team is on track or at risk of delays. They provide visibility into progress, enabling teams to address issues proactively.

In tracking features like the connect assist implementation in Tor Browser for Android, a burndown chart would highlight any lags in progress, allowing timely adjustments to meet release schedules.

GitLab Ultimate provides burndown charts for epics, aiding in tracking larger, multi-milestone goals.

[milestones]: https://docs.gitlab.com/ee/user/project/milestones/
[burndown and burnup charts]: https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html

### Iterations

**Definition**: [Iterations][] are a way to track several issues over a
period of time. For example they could be used for sprints for specific projects (2 weeks iterations). Iteration cadences are containers for iterations and can be used to automate iteration scheduling.

**How we are using them now**: It does not exist in Gitlab Community Edition

**What problem we are solving**: Represent and track in Gitlab the iterations we are having in different projects.

#### Difference between Epics and Iterations

While Epics group related issues to track high-level goals over multiple milestones, Iterations focus on a set timeframe (e.g., two-week sprints) for completing specific tasks within a project. Iterations help teams stay on pace by emphasizing regular progress toward smaller, achievable goals, rather than focusing solely on broad outcomes as Epics do.

Using iterations enables GitLab to mirror Agile sprint cycles directly, adding a cadence to project tracking that can improve accountability and deliverable predictability.

#### Proposal

For projects, we will start planning and tracking issues on iterations
if we get all tickets estimated.

Example: For the VPN project, we have been simulating iterations by tracking implemented features in milestones as we move towards the MVP. This is helpful, but it does not provide the additional functionality that GitLab Iterations provides over milestones. Iterations introduces structured sprint cycles with automated scheduling and cadence tracking. This setup promotes consistent, periodic work delivery, aligning well with development processes. While milestones capture progress toward a major release, iterations allow more granular tracking of tasks within each sprint, ensuring tighter alignment on specific objectives. Additionally, iteration reporting shows trends over time (velocity, backlog management), which milestones alone don't capture.

[Iterations]: https://docs.gitlab.com/ee/user/group/iterations/

### Scoped Labels

**Definition**: [Scoped labels][] are the ones that have a specific domain
and are mutually exclusive. “An issue, merge request, or epic cannot have two scoped labels, of the form key::value, with the same key. If you add a new label with the same key but a different value, the previous key label is replaced with the new label.”

**How we are using them now**: Gitlab Community Edition does not have scoped labels. For Backlog/Next/Doing workflows, we manually add/remove labels.

**What problem we are solving**: We can represent more complex workflows.  Example: We can use scoped labels to represent workflow states. workflow::development, workflow::review and workflow:deployed for example. TPA could use this to track issues per service better, and all teams could use this for the Kanban Backlog/Next/Doing workflow.

[Scoped labels]: https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels

### Issue Management features

Issue weights, linked issues and multiple assignees all enhance the management of epics by improving clarity, collaboration, and prioritization, ultimately leading to more effective project outcomes. 

**Issue Weights**
We can assign [weight to an issue][] to represent value, complexity or anything else that may work for us. We would use issue weights to quantify the complexity or value of tasks, aiding in prioritization and resource allocation. This helps teams focus on high-impact tasks and balance workloads, addressing potential bottlenecks in project execution.

Weights assigned to issues can help prioritize tasks within an epic based on complexity or importance. This allows teams to focus on high-impact issues first, ensuring that the most critical components of the epic are addressed promptly. By using issue weights, teams can also better estimate the overall effort required to complete an epic, aiding in resource allocation and planning.

**Linked Issues**
[Linked issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) enhance clarity by showing dependencies and relationships, improving project tracking within Epics. This ensures teams are aware of interdependencies, which aids in higher level project management. Linked issues can be marked as block by or blocks or related to. Because linked issues can show dependencies between tasks that is particularly useful in the context of epics. Epics often encompass multiple issues, and linking them helps teams understand how the completion of one task affects another, facilitating better project planning and execution. For example, if an epic requires several features to be completed, linking those issues allows for clear visibility into which tasks are interdependent

**Multiple Assignees**
The ability to [assign multiple](https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html) team members to a single issue can foster collaboration within epics. However, it can also complicate accountability, as it may lead to confusion over who is responsible for what. In the context of an epic, where many issues contribute to a larger goal, it's important to balance shared responsibility with clear ownership to ensure that tasks are completed efficiently

The option for [multiple assignees](https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html) could lead to ambiguity about responsibility. It may be beneficial to limit this feature to ensure clear accountability. The multiple assignees feature in GitLab can be turned off at the instance-wide or group-wide level. 

[weight to an issue]: https://docs.gitlab.com/ee/user/project/issues/issue_weight.html 

### Health Status

**Definition**: [health status][] is a feature on issues to mark if an issue is progressing as planned, needs attention to stay on schedule or is at risk. This will help us mark specific issues that needs more attention to not block or delay deliverables of a specific project.

[health status]: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#health-status

### Wiki in groups

**Definition**: [Groups have a wiki][] that can be edited by all group members.

**How we are using them now**: We keep a ‘team’ project in each group to have general documentation related to the group/team.

**What problem we are solving**: The team project usually gets lost inside of each group. A wiki that belongs to the group would give more visibility to the general documentation of the team.

*It is unclear if this feature is something that we may want to use right away. It may need more effort in the migration of the wikis that we have right now and it may not resolve the problems we have with wikis.*

[Groups have a wiki]: https://docs.gitlab.com/ee/user/project/wiki/group.html

## Migration Plan

To move from Gitlab Community Edition to Gitlab Ultimate we need to:

1. Make a decision on which program to apply to in at Gitlab Inc. (see
   the [GitLab programs appendix][] for details on the options). We propose to go with 2. Program Gitlab for Open Source.
2. Establish policy and system to regularly remove inactive GitLab    accounts (note: Tails have a [script to do this][])
3. Request a license to get all the Ultimate features in the self-managed instance that we maintain.
4. [Activate the license code][]

[GitLab programs appendix]: #gitlab-programs
[Activate the license code]: https://docs.gitlab.com/ee/administration/license.html

*Note that, if our submission is approved, it will be only for one year
until we renew it again through the same process/application. We will
need to request it for the amount of users that we have.*

[script to do this]: https://gitlab.tails.boum.org/tails/tails/-/blob/stable/bin/gitlab-users-cleanup?ref_type=heads

## User count evaluation

GitLab license costs depend on the number of users. As of September 2024 we have:

| Total users       | 3281  |
|-------------------|-------|
| Last sign in 2022 | 1180  |
| Never signed in   | 1161  |
| Inactive users    | ~2900 |

In March 2024, anarcat and gaba [evaluated][] this:

| count | last activity |
|-------|---------------|
| 220   | < 30 days     |
| 400   | 6 months      |
| 1000  | ~2 years      |
| 1500  | ~3 years      |
| 2000  | never         |

... estimating we have closer to 200-400 actual, active users in the last 6 months, but 1000 users that were active in the last 2 years.

Therefore, this proposal includes the removal of all users that have
not signed in 2 years.

[evaluated]: https://gitlab.torproject.org/tpo/team/-/issues/202#note_3005482

# Affected users

It affects the whole Tor community and anybody that wants to report an
issue or contribute by tools maintained by the Tor project.

## Personas

All people interacting with Gitlab (Tor project's staff and volunteers) will have to start using a non-free platform for their work and volunteering time. The following list is the different roles that use Gitlab.

### Developer at Tor project

Developers at the Tor project maintain different repositories. They need to:

- understand priorities for the project they are working on as well as
  the tool they are maintaining.
- get their code reviewed by team members.

With Gitlab Ultimate they:

- will be able to have more than 1 person reviewing the MR if needed.
- understand how what they are working on fits into the big picture of
  the project or new feature.
- understand the priorities of the issues they have been assigned to.

### Team lead at Tor Project

Team leads at the Tor project maintain different repositories and
coordinate the work that their team is doing. They need to:

- maintain the roadmap for their team.
- track that the priorities that were set for each person and their
  team is being followed.
- maintain the team's wiki with the right info on what the team does,
  the priorities as well as how volunteers can contribute to it.
- do release management.
- work on allocations with the PM.
- encode deliverables into gitlab issues.

With Gitlab Ultimate, they:

- won't have to maintain a separate wiki project for their team's
  wiki.
- can keep track of the projects that their teams are in without
  having to maintain a spreadsheet outside of Gitlab.
- have more than one reviewer for specific MRs.
- have iterations for the work that is happening in projects.

### Project manager at Tor Project

PMs manage the projects that Tor Project gets funding (or not)
for. They need to:

- collect the indicators that they are tracking for the project's success.
- track progress of the project.
- be aware of any blocker when working on deliverables.
- be aware on any change of the timeline that was setup for the project.
- decide if the deliverable is done.
- understand the reconciliation between projects and teams roadmap.
- work on allocations with the team lead.

With Gitlab Ultimate they:

- track progress of projects in a more efficient way

### Community contributor to Tor

Volunteers want to:

- report issues to Tor.
- collaborate by writing down documentation or processes in the wiki.
- contribute by sending merge requests.
- see what is the roadmap for each tool being maintained.
- comment on issues.

There will be no change on how they use Gitlab.

### Anonymous cypherpunk

Anonymous volunteers want to:

- report issues to Tor in an anonymous way.
- comment on issues
- see what is the roadmap for each tool being maintained

There will be no change on how they use Gitlab.

#### Sysadmins at the Tor project (TPA)

Sysadmins will start managing non-free software after we migrate to
Gitlab Ultimate, something that had only been necessary to handle
proprietary hardware (hardware RAID arrays and SANs, now retired) in
the past.

# Costs

Out of the box, the idea is to get an exception from GitLab.com to get
Ultimate for free. There's also staff costs, detailed separately
below.

## Paying for GitLab Ultimate

But if we stop getting that exception, there's a significant cost we
would need to absorb if we wish to stay on Ultimate.

In [January 2024](https://gitlab.torproject.org/tpo/team/-/issues/202#note_2986964), anarcat made an analysis on the number of users
active then and tried to estimate how much it would cost to cover for
that, using the [official calculator](https://about.gitlab.com/pricing/ultimate/). It added up to 3,8610\$/mth
for 390 "seats" and 3,000 "guest" users, that is 463k\$/year.

If we somehow manage to trim our seat list down to 100, it's still
9,900\$/mth or 120,000\$/year.

Estimating the number of users (and therefore cost) has been
difficult, as we haven't been strict in allocating new users account
(because they were free). Estimates range from 200 to 1000 seats,
[depending on how you count](https://gitlab.torproject.org/tpo/team/-/issues/202#note_3005482).

## Staff estimates

Labour associated to the switch *to* GitLab ultimate is generally
assumed to be trivial: it's one flip to switch in the configuration,
and is not expect to require much involvement from TPA.

All teams will need to adapt to use the new functionality, which will
impose some overhead in the beginning, hopefully leveling or reducing
the time spent in managing issues and projects in GitLab.

## Reverting GitLab Ultimate

*Reverting* GitLab Ultimate changes are much more involved. By using
Epics, scoped labels and so on, we are creating a dependency on
closed-source features that we can't easily pull out of. This would
imply data loss if we cannot find a replacement strategy.

If we *do* try to convert things (for example Epics into Milestones),
it will require a significant of time to write conversion scripts. The
actual time for that work wasn't estimated.

# Timeline

* November 2024: Send this proposal to tor-internal for comments.
* January  2025: Discussion about all comments on the proposal in the
  all hands meeting. Final decision is made by Tor Project's executive director.
* February 2025: Apply to the program "Gitlab for Open Source". TPA starts working on the transition to new codebase.
* March 2025: We start using Gitlab Ultimate.

## Deadline 

Consider this proposal to be discussed and approved by the first all hands meeting in January 2025.

# References

- [Trac to Gitlab migration plan][]
- [GitLab feature comparison][]
- [How we do project management at Tor][]
- [Overview of how a project get funded][]
- Tor's [project life cycle][]

[project life cycle]: https://gitlab.torproject.org/tpo/team/-/wikis/Process/HowProjectManagementAtTheTorProject#whats-the-lifetime-of-a-project
[Trac to Gitlab migration plan]: https://nc.torproject.net/f/7376
[GitLab feature comparison]: https://about.gitlab.com/pricing/feature-comparison/
[How we do project management at Tor]: https://gitlab.torproject.org/tpo/team/-/wikis/process/How-we-do-project-management-at-The-Tor-Project
[Overview of how a project get funded]: https://gitlab.torproject.org/tpo/team/-/wikis/2023-Tor-Meeting-Costa-Rica-Wiki/overview-of-how-projects-get-funded

# Appendix

## Gitlab Programs

There are three possible programs that we could be applying for in Gitlab:

### 1. Program Gitlab for Nonprofits

The GitLab for Nonprofit Program operates on a first come-first served
basis each year. Once they reach their donation limit, the application
is no longer be available.  This licenses must be renewed
annually. Program requirements may change from time to time.

#### Requirements

* Nonprofit registered as 501c3
* Align with [Gitlab Values](https://handbook.gitlab.com/handbook/values/)
* Priorities on organizations that help advance Gitlab’s [social and
  environmental key topics][] (diversity, inclusion
  and belonging, talent management and engagement, climate action and
  greenhouse gas emissions)
* Organization is not registered in China
* Organization is not political or religious oriented.

[social and environmental key topics]: https://about.gitlab.com/handbook/legal/ESG/

#### Benefits from the ‘nonprofit program’ at Gitlab

* Free ultimate license for ONE year (SaaS or self-managed) up to 20 seats. Additional seats may be requested by may not be granted.

_Note: we will add the number of users we have in the request form and [Gilab will reach out](https://forum.gitlab.com/t/number-of-seats-in-the-program-for-a-self-hosted-instance/116746) if there is any issue._

#### How to apply

Follow the [nonprofit program application form][] 

[nonprofit program application form]: https://about.gitlab.com/solutions/nonprofit/join/#nonprofit-program-application

### 2. Program Gitlab for Open Source

Gitlab’s way to support open source projects.

#### Requirements

* Use OSI-approved licenses for their projects. Every project in the
  applying namespace must be published under an OSI-approved open
  source license.
* Not seek profit. An organization can accept donations to sustain its
  work, but it can’t seek to make a profit by selling services, by
  charging for enhancements or add-ons, or by other means.
* Be publicly visible. Both the applicant’s self-managed instance and
  source code must be publicly visible and publicly available.
* Agree with the [GitLab open source program agreement][]

[GitLab open source program agreement]: https://handbook.gitlab.com/handbook/legal/opensource-agreement/

#### Benefits from the ‘nonprofit program’ at Gitlab

* Free ultimate license for ONE year (SaaS or self-managed) with
  50,000 compute minutes calculated at the open source program cost
  factor (zero for public projects in self-manage instances). The
  membership must be renewed annually.

_Note: we will add the number of users we have in the request form and [Gilab will reach out](https://forum.gitlab.com/t/number-of-seats-in-the-program-for-a-self-hosted-instance/116746) if there is any issue._

#### How to apply

Follow the [open source program application form][].

[open source program application form]: https://about.gitlab.com/solutions/open-source/join/#open-source-program-application

### 3. Program Gitlab for Open Source Partners 

The GitLab Open Source Partners program exists to build relationships
with prominent open source projects using GitLab as a critical
component of their infrastructure. By building these relationships,
GitLab hopes to strengthen the open source ecosystem.

#### Requirements

* Engage in co-marketing efforts with GitLab
* Complete a public case study about their innovative use of GitLab
* Plan and participate in joint initiatives and events
* Members of the open source program

#### Benefits from the ‘nonprofit program’ at Gitlab

* Public recognition as a GitLab Open Source Partner
* Direct line of communication to GitLab
* Assistance migrating additional infrastructure to GitLab
* Exclusive invitations to participate in GitLab events
* Opportunities to meet with and learn from other open source partners
* Visibility and promotion through GitLab marketing channels

#### How to apply

It is by invitation only. Gitlab team members can nominate projects as
partners by [opening an issue in the open source partners program][].

[opening an issue in the open source partners program]: https://gitlab.com/groups/gitlab-com/marketing/developer-relations/open-source-program/gitlab-open-source-partners/-/issues
