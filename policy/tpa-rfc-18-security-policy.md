---
title: TPA-RFC-18: Security Policy
costs: N/A
approval: TPA
affected users: TPA
deadline: September 15
status: draft
version: 0.0.1
INFOSEC Status: PUBLIC
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41727
---

[[_TOC_]]

Summary: this policy establishes a security policy for members of the
TPA team.

# Background

Lorem ipsum, TPA needs a security policy.

# Proposal

This RFC de facto proposes the adoption of the current Tails security policies.
The existing Tails policies have been refactored using the TPI templates for 
security policies. Minor additions have been made based on existing policies
within TPI and results of Tails' risk assessment.

## Scope

Note that this proposal applies only inside of TPA, and doesn't answer
the need of a broader Tor-wide security policy, discussed in
[tpo/team#41][].

[tpo/team#41]: https://gitlab.torproject.org/tpo/team/-/issues/41

# TPA Team Security Policies

## Introduction

This document contains the **baseline security procedures** for protecting both
an organization, its employees, it's contributors and the community in general.

It's based on the [Security Policies Template][] from the [OPSEC Templates][]
project version 0.0.2.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to
be interpreted as described in [BCP 14](https://www.rfc-editor.org/info/bcp14).

[Security Policies Template]: https://tpo.pages.torproject.net/operations/opsec-templates/boilerplates/policies/
[OPSEC Templates]: https://tpo.pages.torproject.net/operations/opsec-templates/

## Roles

Each member of the TPA Team can have many roles. The current defined
roles are for the Team are:

* TPA System Administrator (SA): basically everyone within TPA
* TPA Team Lead (TL): our glorious leader

## Threat Levels

A level informs the threat level someone is exposed to by performing some role
in a given context.

Levels are _cumulative_, which means that someone working in a threat level
**1** MUST adopt procedures from levels **0** and **1**, and MAY also
adopt procedures from level **2**.

* **Level 0 - GREEN (LOW RISK)**: that's the baseline level: everyone is
  on this level by default.
* **Level 1 - YELLOW (MEDIUM RISK)**: increased level.
* **Level 2 - RED (HIGH RISK)**: the highest threat level.

The specific level a team members is under for a given role performed should
be assigned during a security assessment.

If a person has many different roles in different threat levels, and with
possibly conflicting procedures, always assume the overall procedures with the
greatest security level, just to be sure.

This threat level system is loosely based on the [Traffic Light Protocol][].

[Traffic Light Protocol]: https://en.wikipedia.org/wiki/Traffic_Light_Protocol

## Information status

These are the currently defined Information Security (INFOSEC) classification
status:

1. **PUBLIC**, such as:
    * Public repositories.
    * Public sites.
    * Released source code.
    * Public interviews, talks and workshops.
    * Public mailing list archives.
    * Public forums.
    * Public chat channels.
2. **PRIVATE**: anything meant only to tor-internal, loss of
   confidentiality would not cause great harm
    * Private [GitLab][] groups/repositories.
    * Confidential tickets.
    * Internal ticket notes.
    * [Nextcloud][].
3. **SECRET**: meant only for TPA, with need-to-know access, loss of
   confidentiality cause great harm or at least significant logistical
   challenges (e.g. mass password rotations)
    * Only on encrypted media (such as a [KeePassXC][] wallet on [Nextcloud][]).

Declassification MUST be decided in a case-by-case basis and never put
people in danger.

It's RECOMMENDED that each document has a version and an INFOSEC status on it's
beginning. This MAY be a application-specific status like a GitLab
issue that's marked as "confidential".

[Nextcloud]: https://nc.torproject.net
[GitLab]: https://gitlab.torproject.org
[KeePassXC]: https://keepassxc.org

## Policies

The following are the **security policies for TPA Team members**.

Each security procedure is attached to a given role and has a _code word_ to
make it easier to refer.

As an example, procedures for _Online Support_ work begins with the `OnSup`
prefix, so the first policy from the level 0 is referred as `OnSup.0.1` etc.

### TPA System Administrators (SA)

#### Level 0 - GREEN - LOW

* REQUIRED: **Organization-wide policies** (`pid:general_security_policies`) (`ver:0.0.2`):
    1. Follow any existing organization-wide, baseline security policies.

* RECOMMENDED: **Pseudonyms** (`pid:privacy_pseudonyms`) (`ver:0.0.1`):
    1. When joining the organization or a team, tell people that they can use
       pseudonyms.

* RECOMMENDED: **Policy reviews** (`pid:assessment_policy_reviews`) (`ver:0.0.1`):
    1. During onboard, make the newcomers to be the reviewers of the security
       policies, templates and HOWTOs for one month, and encourage them to submit
       merge requests to fix any issues and outdated documentation.

* REQUIRED: **Full Disk Encryption (FDE) for Workstations** (`pid:fde_workstation`) (`ver:0.0.1`):
    1. Use an acceptable Full Disk Encryption technology in your workstation
       (be it a laptop or a desktop).
    2. Encryption passphrase SHOULD be considered strong and MUST NOT be used
       for other purposes.

* REQUIRED: **Physical access** (`pid:computer_physical_access`) (`ver:0.0.1`):
    1. To protect your data from getting stolen offline:
         - be careful about the physical security of your hardware.
         - do not leave your workstation unlocked and unattended.

* REQUIRED: **Email provider** (`pid:email_provider`) (`ver:0.0.1`):
    1. To protect e-mail from online sniffing, make sure your email provider:
         - uses reasonably modern encryption by default, at least for mail in
           transit
         - does not hand over mail data to third parties unless forced to do so
           by a court order
         - does not spy on its users (e.g., analyse mail data to form user
           profiles)

* REQUIRED: **Handling of cryptography material** (`pid:crypto_material`) (`ver:0.0.1`):
    1. Adopt safe procedures for handling key material (Onion Service keys,
       HTTPS certificates, SSH keys etc), including generation, storage,
       transmission, sharing, rollover, backing up and destruction.

* REQUIRED: **Password manager** (`pid:infosec_password_manager`) (`ver:0.0.1`):
    1. Use a secure password manager to store all credentials related to your
       work at Tor.
    2. Generate unique long random strings to use as passwords.
    3. Do not re-use passwords across services.
    4. To prevent phishing attacks, use a browser plugin for your password
       manager.

* REQUIRED: **Screensaver** (`pid:computer_screensaver`) (`ver:0.0.1`):
    1. Use a locking screensaver on your workstation.

* REQUIRED: **Device Security for Travels** (pid:travel_device) (ver:0.0.2):

    1. Turn off all your device before any border crossings or security
       checkpoints. It will take some time for DRAM to lose its content.
    2. Do not input information into any device touched by a bad actor even if
       you got the device back, it might have been backdoored. You could try to
       get your information out of it, but do not input any new information into
       it. Full disk encryption have limited protection for the data integrity.
    3. Make sure the devices you don't bring stay protected (at home or in good
       hands) so it's hard to physically compromise them while you're away.

* REQUIRED: **Firewall** (`pid:computer_firewall`) (`ver:0.0.1`):
    1. Use a firewall to block incoming traffic;
    2. You MAY make an exception to allow SSH-ing from another machine that
       implements the same security level.

* OPTIONAL: **Software isolation** (`pid:computer_software_isolation`) (`ver:0.0.3`):
    1. Use desktop isolation/sandboxing whenever possible (such as Qubes)
       (which threat models and roles it would apply etc), but not imposing
       this as a requirement.
    2. Use a Mandatory Access Control system such as AppArmor.

#### Level 1 - YELLOW - MEDIUM

* REQUIRED: **Hardware Security Tokens** (`pid:computer_tokens`) (`ver:0.0.1`):
    1. Use a Hardware Security Token:
        1.1. For Yubikeys, refer to the
             [Yubikey HOWTO](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/yubikey).

* REQUIRED: **Secure software** (`pid:computer_secure_software`) (`ver:0.0.1`):
    1. Ensure all the software that you run in your system, and all firmware
       that you install is either:
         - Non-free firmware shipped by Debian packages that are included
           in the non-free-firmware repository.
         - Free software installed from trustworthy repositories via mechanisms
           that have good cryptographic properties. Such software should also
           come with:
             - A similarly secure automatic update mechanism
             - A similarly secure update notification mechanism for you to keep
               it up-to-date manually.
           For example, you can use up-to-date Debian stable or Debian unstable
           and ensure you have configured unattended upgrades, but Debian 
           testing has no reliable security support, so it is not good enough.
         - Isolated using either:
           - A virtual machine
           - A desktop session separate from the one that has the privileged
             access this policy is about, running under a non-privileged UID
             (not sudoer and not PolicyKit admin for example).
           - Flatpak with sandboxing enabled, running inside a Wayland
             desktop session.
           - Snap, running inside a Wayland desktop session, on a system with
             AppArmor enabled.
           - Containers (Docker, LXC, you name it).
             You SHOULD configure your containers to make it harder for
             malicious code to escape the container… and to make the
             consequences of a successful escape less catastrophic.
         - Audited by yourself when you install it and on every update.
       Examples:
         - A Firefox add-on from addons.mozilla.org come from a trustworthy
           repository with cryptographic signatures on top of HTTPS, and you
           get notified of updates, so it's OK.
         - "go get BLAH" recursively pulls code from places that are probably
           not all trustworthy. The security of the mechanism entirely relies
           purely on HTTPS. So, isolate the software or audit the dependency
           tree. If you choose "audit", then set up something to ensure
           you'll keep it up-to-date, then audit every update.
           Same goes for PIP, Gem, npm, and friends, unless you show that the
           one you use behaves better.
         - Software installed via Git: checking signed tags made by
           people/projects you trust is OK but then you must either set up
           something to regularly check yourself for updates, or isolate.
           If verifying signed tags is not possible, then isolate or audit
           the software.

* REQUIRED: **Travel avoidances** (`pid:travel_avoidances`) (`ver:0.0.1`):
    1. You MUST NOT take your workstation, nor your security hardware token, to
       any country where association with circumvention technology may get you
       in legal trouble. This includes any country that blocks or has blocked
       tor traffic.

#### Level 2 - RED - HIGH

N/A

# Examples

Examples:

 * ...

Counter examples:

 * ...

# References

Internal:

* [TPA-RFC-18: discussion ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41727)
* [Tor-wide security policy discussion][tpo/team#41]
* [NetworkTeam/SecurityPolicy page (Legacy)](https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/SecurityPolicy)
* [TPA-RFC-7: root access](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-7-root)
* [TPA-RFC-17: Establish a global disaster recovery plan (#40628)](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40628)

External:

* [OpenSSL Security Policy](https://www.openssl.org/policies/secpolicy.html)
* [Linux Foundation: Useful IT policies](https://github.com/lfit/itpol/)
