---
title: TPA-RFC-69: switch to HTTP basic auth on CiviCRM server
costs: N/A
approval: TPA, web folks
affected users: all CiviCRM users
deadline: 2024-09-29
status: standard
discussion: https://gitlab.torproject.org/tpo/web/civicrm/-/issues/147
---

[[_TOC_]]

Summary: switch authentication method for CiviCRM server, which
implies a password reset for all users.

You are receiving this because you are TPA or because you have such a
password.

# Background

The CiviCRM server is currently protected by two layers of
authentication:

 1. webserver-level authentication, a first username/password managed
    by TPA, using a mechanism called "HTTP Digest"

 2. application-level authentication, a *second* username/password
    managed by the Drupal/CiviCRM administrators (and also TPA)

While trying to hook up the CiviCRM server to the Prometheus
monitoring system ([tpo/web/civicrm#78,][]), we blocked on
Prometheus' [lack of support for HTTP Digest authentication][], that
first layer.

[tpo/web/civicrm#78,]: https://gitlab.torproject.org/tpo/web/civicrm/-/issues/78,
[lack of support for HTTP Digest authentication]: https://github.com/prometheus/common/issues/352

## Security digression

One major downside of htdigest that i didn't realize before is that
the password is stored on disk as a MD5 checksum of the user, realm
and password. This is what's used to authenticate the user and is
essentially the secret token used by the client to authenticate with
the server.

In other words, if someone grabs that htdigest file, they can replay
those passwords as they want. With basic auth, we don't have that
problem: the passwords are hashed, and the hash is *not* used in
authentication: the client sends the plain text password (which can be
sniffed, of course, but that requires an active MITM), and that's
checked against the *hashed* password.

The impact of this change, security wise, is therefore considered to
be an *improvement* to the current system.

# Proposal

Switch the first password authentication layer to regular HTTP
authentication.

This requires resetting everyone's passwords, which will be done by
TPA, and passwords will be communicated to users individually,
encrypted.

For what it's worth, there are 18 users in that user database now (!),
including at least 4 bots (prefixed `cron-` and one called
`frontendapi`). Now that we switched to donate-neo, it might be good
to kick everyone out and reset all of this anyways.

# Alternatives considered

For now, we've worked around the issue by granting the monitoring
server password-less access to the CiviCRM application (although
Drupal-level authentication is still required).

We have tried to grant access to *only* the monitoring endpoint, but
this failed because of the way Drupal is setup, through the
`.htaccess`, which makes such restrictions impossible at the server
level.

# References

See the discussion in [tpo/web/civicrm#147][].

[tpo/web/civicrm#147]: https://gitlab.torproject.org/tpo/web/civicrm/-/issues/147
