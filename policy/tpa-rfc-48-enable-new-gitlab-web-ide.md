---
title: TPA-RFC-48: Enable new GitLab Web IDE
costs: None
approval: TPA
affected users: GitLab users
deadline: 2023-02-28
status: standard
discussion: https://gitlab.torproject.org/tpo/web/lego/-/issues/51
---

[[_TOC_]]

Summary: enable the new VSCode-based GitLab Web IDE, currently in beta, as the
default in our GitLab instance

# Background

The current Web IDE has been the cause of some of the woes when working with the
blog. The main problem was that is was slow to load some of the content of the
in the project repository, and in some cases even crashing the browser.

The [new Web IDE][] announced a few months ago is now available in the version of
GitLab we're running, and initial tests with it seem very promising. The hope is
that it will be much faster than its predecessor, and using it will eliminate
one of the pain points identified by Tor people who regularly work on the blog.

[new Web IDE]: https://about.gitlab.com/blog/2022/05/23/the-future-of-the-gitlab-web-ide/

# Proposal

Make the new Web IDE the default by enabling the `vscode_web_ide` feature flag
in GitLab.

## Affected users

All GitLab users.

## Alternatives

Users who wish to continue using the old version of the Web IDE may continue to
do so, by [adjusting their preferences][].

The removal of the old Web IDE is currently planned for the 16.0 release, which
is due in May 2023.

[adjusting their preferences]: https://docs.gitlab.com/ee/user/project/web_ide_beta/index.html#stop-using-the-web-ide-beta

# Approval

Needs approval from TPA.

# Deadline

The setting is currently enabled. Feedback on this RFC is welcome until Tuesday,
February 28, at which point this RFC will transition to the `standard` state
unless decided otherwise.

# Status

This proposal is currently in the `standard` state.

It will transition naturally to the `obsolete` status once the legacy Web IDE is
removed from GitLab, possibly with the release of GitLab 16.0.

# References

 * [GitLab documentation about the new Web IDE][]
 * discussion ticket: [tpo/web/lego#51][]

[GitLab documentation about the new Web IDE]: https://docs.gitlab.com/ee/user/project/web_ide_beta/index.html#use-the-web-ide-beta

[tpo/web/lego#51]: https://gitlab.torproject.org/tpo/web/lego/-/issues/51
