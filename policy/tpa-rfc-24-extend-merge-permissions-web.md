---
title: TPA-RFC-24: Extend merge permissions for web projects
approval: PMs, web, TPA
affected users: web team, web publishers
status: standard
discussion: https://gitlab.torproject.org/tpo/web/team/-/issues/37
---

[[_TOC_]]

# Background

Currently, when members of other teams such as comms or applications want to
publish a blog post or a new software release, they need someone from the web
team (who have Maintainer permissions in tpo/web projects) to accept (merge)
their Merge Request and also to push the latest CI build to production.

This process puts extra load on the web team, as their intervention is required
on all web changes, even though some changes are quite trivial and should not
require any manual review of MRs. Furthermore, it also puts extra load on the
other teams as they need to follow-up at different moments of the publishing
process to ensure someone from the web team steps in, otherwise the process is
blocked.

In an effort to work around these issues, several contributors were granted the
**Maintainer** role in the `tpo/web/blog` and `tpo/web/tpo` repositories.

# Proposal

I would like to propose to grant the members of web projects who regularly
submit contributions the power to accept Merge requests.

This change would also allow them to trigger manual deployment to production.
This way, we will avoid blocking on the web team for small, common and regular
website updates. Of course, the web team will remain available to review all
the other, more substantial or unusual website updates.

To make this change, under each project's `Settings -> Repository -> Protected
branches`, for the `main` branch, the **Allowed to merge** option would change
from `Maintainers` to `Maintainers + Developpers`. **Allowed to push** would
remain set to `Maintainers` (so Developers would still always need to submit
MRs).

In order to ensure no one is granted permissions they should not have, we
should, at the same time, verify that only core contributors of the Tor Project
are assigned Developer permissions on these projects.

Contributors who were granted the `Maintainer` role solely for the purpose of
streamlining content publication will be switched to the `Developper` role, and
current members with the `Developper` role will be switched to `Reporter`.

## Scope

Web projects under [tpo/web](/tpo/web) which have regular contributors outside
the web team:

 * [tpo/web/blog](/tpo/web/blog)
 * [tpo/web/tpo](/tpo/web/tpo)

# Alternatives considered

An alternative approach would be to instead grant the **Maintainer** role to
members of other teams on the web projects.

There are some inconveniences to this approach, however:

 * The **Maintainer** role grants several additional [permissions][] that we
   should not or might not want to grant to members of other teams, such as the
   permission to manage Protected Branches settings

 * We will end up in a situation where a number of users with the **Maintainer**
   role in these web projects will not be true maintainers, in the sense that
   they will not become responsible for the repository/website in any sense. It
   will be a little more complicated to figure out who are the true maintainers
   of some key web projects.

[permissions]: https://docs.gitlab.com/ee/user/permissions.html

# Timeline

There is no specific timeline for this decision.

# References

 * [issue tpo/web/team#37][]: ongoing discussion

[issue tpo/web/team#37]: https://gitlab.torproject.org/tpo/web/team/-/issues/37
