---
title: TPA-RFC-X: RFC template
costs: 
approval: 
affected users: 
deadline: 
status: draft
discussion: 
---

[[_TOC_]]

Summary: this template describes the basic field a TPA-RFC policy
should have. Refer to [policy/tpa-rfc-1-policy](policy/tpa-rfc-1-policy) for the actual policy requirements.

# Background

# Proposal

## Goals

<!-- include bugs to be fixed -->

### Must have

### Nice to have

### Non-Goals

## Tasks

## Scope

## Affected users

### Personas impact

## Timeline

## Costs estimates

### Hardware

### Staff

# Alternatives considered

# References

See [policy/tpa-rfc-1-policy](policy/tpa-rfc-1-policy).

## Personas description
