---
title: TPA-RFC-78: Dangerzone retirement
status: proposed
discussion: https://gitlab.torproject.org/tpo/tpa/dangerzone-webdav-processor/-/issues/25#note_3153294
---

[[_TOC_]]

Summary: TPA is planning to retire the Dangerzone WebDAV processor
service. It's the bot you can share files with on Nextcloud to
sanitize documents. It has already been turned off and the service
will be fully retired in a month.

# Background

The [dangerzone service][] was established in 2021 to avoid hiring
committees to open untrusted files from the internet. 

 [dangerzone service]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/dangerzone

We've had numerous problems with this, including reliability and
performance issues, the latest of which were possibly us hammering the
Nextcloud server needlessly.

The service seems largely unused: in the past year, only five files or
folders were processed by the service.

Since the service was deployed, the original need has largely been
supplanted, as we now use a third-party service (Manatal) to process
job applications.

Today, the service was stopped, partly to confirm it's not being used.

# Proposal

Fully retire the Dangerzone service. In one month from now, the
virtual machine would be shutdown and the backups deleted another
month after that.

## Timeline

- 2025-01-28 (today): service stopped
- 2025-02-28 (in a month): virtual machine destroyed
- 2025-03-28 (in two months): backups destroyed

# Alternatives considered

## Recovering the service after retirement

If we change our mind, it's possible to restore the service, to a
certain extent.

The machine setup is *mostly* automated: restoring the service
involves creating a virtual machine, a bot account in Nextcloud, and
sharing the credentials with our configuration management.

But the service would need lots of work to be restored to proper
working order, however, and we do not have the resources to do so at
the moment.

# References

Comments welcome by email or in <https://gitlab.torproject.org/tpo/tpa/dangerzone-webdav-processor/-/issues/25>.
