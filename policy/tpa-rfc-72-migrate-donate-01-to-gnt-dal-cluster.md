---
title: TPA-RFC-72: Migrate donate-01 to gnt-dal cluster
costs: N/A
approval: TPA
affected users: all donate.torproject.org users
deadline: 2024-10-02 14:00UTC
status: obsolete
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41775
---

[[_TOC_]]

Summary: donation site will be down for maintenance on Wednesday
around 14:00 UTC, equivalent to 07:00 US/Pacific, 11:00
America/Sao_Paulo, 10:00 US/Eastern, 16:00 Europe/Amsterdam.

# Background

We're having [latency issues][] with the main donate site. We hope
that migrating it from our data center in Germany to the one in Dallas
will help fix those issues as it will be physically closer to the rest
of the cluster.

[latency issues]: https://gitlab.torproject.org/tpo/web/donate-neo/-/issues/134

# Proposal

Move the `donate-01.torproject.org` virtual machine, responsible for
the production <https://donate.torproject.org/> site, between the two
main Ganeti clusters, following the procedure detailed in [#41775][].

[#41775]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41775

Outage is expected to take no more than two hours, but no less than 15
minutes.

# References

See the discussion issue for more information and feedback:

<https://gitlab.torproject.org/tpo/tpa/team/-/issues/41775>
