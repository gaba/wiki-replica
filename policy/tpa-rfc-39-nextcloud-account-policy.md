---
title: TPA-RFC-39: Nextcloud user account policy
approval: Nextcloud administrators, TPA
status: standard
discussion: tpo/tpa/nextcloud#10
---

[[_TOC_]]

Summary: This policy defines who is entitled to a user account on the Tor
Project Nextcloud instance.

# Background

As part of proper security hygiene we must limit who has access to the Tor
Project infrastructure.

# Proposal

Nextcloud user accounts are available for all [Core Contributors](https://gitlab.torproject.org/tpo/community/policies/-/blob/HEAD/membership.txt).
Other accounts may be created on a case-by-case basis. For now, bots are the only exception, and the dangerzone-bot is the only known bot to be in operation.
