---
title: TPA-RFC-5: GitLab migration
costs: staff
approval: TPA
affected users: TPA
deadline: 2020-06-18
status: standard
discussion: https://bugs.torproject.org/34437
---

Summary: the TPA team will migrate its bugtracker and wiki to GitLab,
using Kanban as a planning tool.

# Background

TPA has a number of tools at its disposal for documentation and
project tracking. We currently use email, Trac and ikiwiki. Trac will
be shutdown by the end of the week (at the time of writing) so it's
time to consider other options.

# Proposal

This document proposes to switch to GitLab to track issues and project
management. It also suggests converting from ikiwiki to GitLab wiki in
the mid- to long-term.

## Scope

The scope of this proposal is only within the Tor sysadmin team (TPA)
but could serve as a model for other teams stuck in a similar situation.

This does *not* cover migration of Git repositories which remain
hosted under gitolite for this phase of the GitLab migration.

## Tickets: GitLab issues

As part of the grand GitLab migration, Trac will be put read-only and
we will no longer be able to track our issues there. Starting with the
GitLab migration, all issues should be submitted and modified on
GitLab, not Trac.

Even though it is technically possible for TPA members to bypass the
readonly lock on Trac, this exception will *not* be done. We also wish
to turn off this service and do *not* want to have two sources of
truth!

Issues will be separated by sub-projects under the `tpo/tpa` GitLab
group, with one project per Trac component. But new sub-projects could
eventually be created for specific projects.

## Roadmap: GitLab boards

One thing missing from GitLab is the equivalent of the Trac inline
reports. We use those to organise our monthly roadmap within the team.

There are two possible alternatives for this. We could use the GitLab
"milestones" feature designed to track software releases. But it is
felt we do not really issue "releases" of our software, since we have
too many moving parts to cohesively release those as a whole.

Instead, it is suggested we adopt the [Kanban][] development strategy
which is implemented in GitLab as [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html). 

### Triage

Issues first land into a queue (`Open`), then get assigned to
a specific queue as the ticket gets planned. 

We use the ~Icebox, ~Backlog, ~Next, and ~Doing of the global
"TPO" group board labels. With the `Open` and `Closed` queues, this
gives us the following policy:

 * `Open`: un-triaged ticket
 * ~Icebox: ticket that is stalled, but triaged
 * ~Backlog: planned work for the "next" iteration (e.g. "next month")
 * ~Next: work to be done in the current iteration or "sprint"
   (e.g. currently a month, so "this month")
 * ~Doing: work being done right now (generally during the day or
   week)
 * `Closed`: completed work

That list can be adjusted in the future without formally reviewing
this policy.

The `Open` board should ideally be always empty: as soon as a ticket
is there, it should be processed into some other queue. If the work
needs to be done urgently, it can be moved into the ~Doing queue, if
not, it will typically go into the ~Next or ~Backlog queues.

Tickets should *not* stay in the ~Next or ~Doing queues for long and
should instead actively be closed or moved back into the ~Icebox or
~Backlog board. Tickets should *not* be moved back to the `Open`
board once they have been triaged.

Tickets moved to the ~Next and ~Doing queues should normally be
assigned to a person. The person doing triage should make sure the
assignee has availability to process the ticket before assigning.

Items in a specific queue can be prioritized in the dashboard by
dragging items up and down. Items on top should be done before items
at the bottom. When created in the `Open` queue, tickets are processed
in FIFO (First In, First Out) order, but order in the other queues is
typically managed manually.

[Kanban]: https://en.wikipedia.org/wiki/Kanban_(development)

Triage should happen at least once a week. The person responsible for
triage should be documented in the topic of the IRC channel and rotate
every other week.

## Documentation: GitLab wiki

We are currently using ikiwiki to host our documentation. That has
served us well so far: it's available as a static site in the static
mirror system and allows all sysadmins to have a static, offsite copy
of the documentation when everything is down. 

But ikiwiki is showing its age. it's an old program written in Perl,
difficult to theme and not very welcoming to new users. for example,
it's impossible for a user unfamiliar with git to contribute to the
documentation. It also has its own unique Markdown dialect that is not
used anywhere else. and while Markdown itself is not standardized and
has lots of such dialects, there is /some/ convergence around
CommonMark and GFM (GitHub's markdown) as de-facto standards at least,
which ikiwiki still has to catchup with. It also has powerful macros
which are nice to make complex websites, but do not render in the
offline documentation, making us dependent on the rendered copy (as
opposed to setting up client-side tools to peruse the documentation).

GitLab wikis, in contrast, have a web interface to edit pages. It
doesn't have the macros ikiwiki has, but that's nothing a few
commandline hacks can't fix... or at least we should consider it. They
don't have macros or any more powerful features that ikiwiki has, but
maybe that's exactly what we want.

# Deadline

The migration to GitLab issues has already been adopted in the June
TPA meeting.

The rest of this proposal will be adopted in one week unless there are
any objections (2020-06-18).

Note that the issue migration will be actually done during the GitLab
migration itself, but the wiki and kanban migration do not have an
established timeline and this proposal does *not* enforce one.

# References

 * [old ikiwiki](https://help.torproject.org/)
 * [new TPA group](https://gitlab.torproject.org/tpo/tpa)
 * [GitLab wiki migration issue 34437](https://bugs.torproject.org/34437)
 * GitLab upstream documentation:
   * [issue boards documentation](https://docs.gitlab.com/ee/user/project/issue_board.html)
   * [issue boards feature page](https://about.gitlab.com/stages-devops-lifecycle/kanban-boards/)
   * [issue boards tutorial](https://about.gitlab.com/blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/)
 * [Wikipedia article about Kanban development](https://en.wikipedia.org/wiki/Kanban_(development))
 * [No bullshit article about issue trackers](https://almad.blog/essays/no-bugs-just-todos/) which inspired the
   switch to Kanban
 * [TPA-RFC-19: GitLab labels](policy/tpa-rfc-19-gitlab-labels): policy on service-specific labels
