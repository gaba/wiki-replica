---
title: TPA-RFC-32: Nextcloud root-level shared folders migration
costs: staff
approval: Nextcloud admins
affected users: Nextcloud users, Nextcloud sysadmins
status: standard
discussion: tpo/tpa/nextcloud#1
---

[[_TOC_]]

# Background

In the Tor Project [Nextcloud][] instance, most root-level shared folders
currently exist in the namespace of a single Nextcloud user account. As such,
the management of these folders rests in the hands of a single person, instead
of the team of [Nextcloud administrators][].

In addition, there is no folder shared across all users of the Nextcloud
instance, and incoming file and folder shares are created directly in the root
of each user's account, leading to a cluttering of users' root folders. This
clutter is increasingly restricting the users' ability to use Nextcloud to its
full potential.

[Nextcloud]: https://nc.torproject.net
[Nextcloud administrators]: https://nc.torproject.net/settings/users/admin

# Proposal

## Move root-level shared folders to external storage

The first step is to activate the `External storage support` Nextcloud app.
This app is among those shipped and maintained by the Nextcloud core
developers.

Then, in the Administration section of Nextcloud, we'll create a series of
"Local" external storage folders and configure sharing as described in the
table below:

| Source namespace | Source folder          | New folder name         | Share with            |
| ---------------- | ---------------------- | ----------------------- | --------------------- |
| gaba             |  Teams/Anti-Censorship | Anti-censorship Team    | Anti-censorship Team  |
| gaba             |  Teams/Applications    | Applications Team       | Applications Team     |
| gaba             |  Teams/Communications  | Communications Team     | Communications        |
| gaba             |  Teams/Community       | Community Team          | Community Team        |
| Al               |  Fundraising           | Fundraising Team        | Fundraising Team (new)|
| gaba             |  Teams/Grants          | Fundraising Team/Grants | (inherited)           |
| gaba             |  Teams/HR (hiring, etc)| HR Team                 | HR Team               |
| gaba             |  Teams/Network         | Network Team            | Network Team          |
| gaba             |  Teams/Network Health  | Network Health Team     | Network Health        |
| gaba             |  Teams/Sysadmin        | TPA Team                | TPA Team              |
| gaba             |  Teams/UX              | UX Team                 | UX Team               |
| gaba             |  Teams/Web             | Web Team                | Web Team (new)        |

## Create "TPI" and  "Common" shared folders

We'll create a shared folder named "Common", shared with all Nextcloud users,
and a "TPI" folder shared with all TPI employees and contractors.

 * **Common** would serve as a repository for documents of general interest,
   accessible to all TPO Nextcloud accounts, and a common space to share
   documents that have no specific confidentiality requirements

 * **TPI** would host documents of interest to TPI personnel, such as holiday
   calendars and the employee handbook

## Set system-wide default incoming shared folder to "Incoming"

Currently when a Nextcloud user shared documents or folders with another user
or group of users, those appear in the share recipients' root folder.

By making this change in the Nextcloud configuration (`share_folder`
parameter), users who have not already changed this in their personal
preferences will receive new shares in that subfolder, instead of the root
folder. It will not move existing files and folders, however.

## Reorganise shared folders and documents

Once the preceding changes are implemented, we'll ask Nextcloud users to
examine their list of "shared with others" files and folders and move those
items to one of the new shared folders, where appropriate.

This should lead to a certain degree of consolidation into the new team and
common folders.

## Goals

 * Streamline the administration of team shared folders
 * De-clutter users' Nextcloud root folder

## Scope

The scope of this proposal is the Nextcloud instance at
https://nc.torproject.net
