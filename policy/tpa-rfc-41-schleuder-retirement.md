---
title: TPA-RFC-41: Schleuder retirement
costs: staff
approval: TPA, community council, security team.
affected users: schleuder users
deadline: None
status: rejected
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40564
---

[[_TOC_]]

Summary: replace Schleuder with GitLab or regular, TLS-encrypted
mailing lists

# Background

Schleuder is a mailing list software that uses OpenPGP to encrypt
incoming and outgoing email. Concretely, it currently hosts five (5)
mailing lists which include one for the community council, three for
security issues, and one test list.

There are major usability and maintenance issues with this service and
TPA is considering removing it.

## Issues 

 * Transitions within teams are hard. When there are changes inside
   the community council, it's difficult to get new people in and
   out.

 * Key updates are not functional, partly related to the meltdown of
   the old OpenPGP key server infrastructure

 * even seasoned users can struggle to remember how to update their
   key or do basic tasks

 * Some mail gets lost. some users write email that never gets
   delivered, the mailing list admin gets the bounce, but not the list
   members which means critical security issues can get misfiled

 * Schleuder only has one service admin

 * the package is actually deployed by TPA, so service admins only get
   limited access to the various parts of the infrastructure necessary
   to make it work (e.g. they don't have access to Postfix)

 * Schleuder doesn't *actually* provide "end-to-end" encryption:
   emails are encrypted to the private key residing on the server,
   then re-encrypted to the current mailing list subscribers

 * Schleuder attracts a lot of spam and encryption makes it possibly
   harder to filter out spam

# Proposal

It is hereby proposed that Schleuder is completely retired from TPA's
services. Two options are given by TPA as replacement services:

 1. the security folks migrate to GitLab confidential issues, with the
    understanding we'll work on the notifications problems in the long
    term
    
 2. the community council migrates to a TLS-enforced, regular Mailman
    mailing list

## Rationale

The rationale for the former is that it's basically what we're going
to do anyways; it looks like we're not going to continue using
Schleuder for security stuff anyways, which leaves us with a single
consumer for Schleuder: the community council. There, I propose we
setup a special mailman mailing list that will have similar properties
to Schleuder:

 * no archives
 * no moderation (although that could be enabled of course)
 * subscriptions requires approval from list admins
 * transport encryption (by enforced TLS at the mail server level)
 * possibility of leakage when senders improperly encrypt email
 * email available in cleartext on the mailserver while in transit

The main differences from Schleuder would be:

 * no encryption at rest on the clients
 * no "web of trust" trust chain; a compromised CA could do an active
   "machine in the middle" attack to intercept emails
 * there may be gaps in the transport security; even if all our
   incoming and outgoing mail uses TLS, a further hop might not use it

That's about it: that's all Schleuder gives us compared to an OpenPGP
based implementation.

# Personas

TODO: make personas for community council, security folks, and the peers
that talk with them.

# Alternatives considered

Those are the current known alternatives to Schleuder that are
currently under consideration.

## Discourse

We'd need to host it, and even then we only get transport encryption,
no encryption at rest

## GitLab confidential issues

In [tpo/team#73][], the security team is looking at using GitLab
issues to coordinate security work. Right now, confidential issues are
still sent in cleartext ([tpo/tpa/gitlab#23][]), but this is something
we're working on fixing (by avoiding sending a notification at all, or
just redacting the notification).

[tpo/team#73]: https://gitlab.torproject.org/tpo/team/-/issues/73
[tpo/tpa/gitlab#23]: https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/23

This is the solution that seems the most appropriate for the security
team for the time being.

## Improving Schleuder

 * install the [schleuder web interface][], which makes some
   operations easier

 * [autorefresh keys][]

 * possibly [integrate Schleuder into GitLab][]

 * do not encrypt incoming cleartext email

[schleuder web interface]: https://0xacab.org/schleuder/schleuder-web
[autorefresh keys]: https://schleuder.org/schleuder/docs/server-admins.html#maintenance
[integrate Schleuder into GitLab]: https://0xacab.org/schleuder/schleuder-gitlab-ticketing

## Mailman with mandatory TLS

A mailing list *could* be downgraded to a plain, unencrypted Mailman
mailing list. It should be a mailing list without archives,
un-moderated, but with manual approval for new subscribers, to best
fit the current Schleuder implementation.

We could enforce TLS transport for incoming and outgoing mail on that
particular mailing list. According to [Google's current transparency
report][] (as of 2022-10-11), between 77% and 89% of Google's
outbound mail is encrypted and between 86% to 93% of their inbound
mail is encrypted. This is up from about 30-40% and 30% (respectively)
when they started tracking those numbers in January 2014.

Email would still be decrypted at rest but it would be encrypted in
transit.

[Google's current transparency report]: https://transparencyreport.google.com/safer-email/overview?hl=en

## Mailman 3 and OpenPGP plugin

Unfinished, probably unrealistic without serious development work in
Python

## Matrix 

Use encrypted matrix groups

## RT 

it supports OpenPGP pretty well, but stores stuff in cleartext, so
also only transport encryption.

## Role key and home-made Schleuder

This is some hack with a role email + role OpenPGP key that remails
encrypted email, kind of a poor-man's schleuder. Could be extremely
painful in the long term. I believe there are existing remailer
solutions for this like this in Postfix.

A few solutions for this, specifically:

 * [kuvert][] - sendmail wrapper to encrypt email to a given OpenPGP
   key, Perl/C
 * [koverto][] - similar, written in Rust with a Sequoia backend
 * [jak: Encrypted Email Storage, or DIY ProtonMail][] - uses Dovecot Sieve filters to encrypt a
   mailbox
 * [Perot: Encrypt specific incoming emails using Dovecot and
   Sieve][] - similar

[kuvert]: https://github.com/az143/kuvert
[koverto]: https://gitlab.com/koverto/koverto
[jak: Encrypted Email Storage, or DIY ProtonMail]: https://blog.jak-linux.org/2019/06/13/encrypted-email-storage/
[Perot: Encrypt specific incoming emails using Dovecot and    Sieve]: https://perot.me/encrypt-specific-incoming-emails-using-dovecot-and-sieve

## Shared OpenPGP key alias

Another option here will be to have an email alias and to share the
private key between all the participants of the alias. No technology
involved in the server or private material there. But a bit more
complicated to rotate people (mostly if you stop trusting them) and a
lot of trust in place for the members of the alias.

## Signal groups

This implies "not email", leaking private phone numbers, might be
great for internal discussions, but probably not an option for
public-facing contact addresses.

Maybe a front phone number could be used as a liaison to get encrypted
content from the world?

# Alternatives not considered

Those alternatives came *after* this proposal was written and
evaluated..

## GnuPG's ADSK

In March 2023, the GnuPG projected [announced ADSK][], a way to tell
other clients to encrypt to multiple of *your* keys. It doesn't
actually answer the requirement of a "mailing list" per se, but could
make role keys easier to manage in the future, as each member could
have their own subkey.

[announced ADSK]: https://gnupg.org/blog/20230321-adsk.html
