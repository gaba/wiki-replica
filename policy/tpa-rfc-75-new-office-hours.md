---
title: TPA-RFC-75: new office hours
costs: N/A
approval: TPA
affected users: TPA
deadline: 2024-11-27
status: obsolete
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41882
---

[[_TOC_]]

Summary: revive the "office hours", in a more relaxed way, 2 hours on
Wednesday (14:00-16:00UTC, before the all hands).

# Background

In [TPA-RFC-34][] we declared the "[End of TPA office hours][TPA-RFC-34]", arguing that:

[TPA-RFC-34]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-34-office-hours-ends

> This practice didn't last long, however. As early at December 2021, we
> noted that some of us didn't really have time to tend to the office
> hours or when we did, no one actually showed up. When people *would*
> show up, it was generally planned in advance.
> 
> At this point, we have basically given up on the practice.

# Proposal

Some team members have expressed the desired to work together more,
instead of just crossing paths in meetings.

Let's assign a 2 hours time slot on Wednesday, where team members are
*encouraged* (but don't *have* to) join to work together.

The proposed time slot is on Wednesday, 2 hours starting at 14:00 UTC,
equivalent to 06:00 US/Pacific, 11:00 America/Sao_Paulo, 09:00
US/Eastern, 15:00 Europe/Amsterdam.

This is the two hours before the all hands, essentially.

The room would be publicly available as well, with other people free
to join in to ask for help, although they might be broken out to break
out rooms for more involved sessions.

## Technical details

Concretely, this involves:

 1. Creating a recurring event in the TPA calendar for that time slot

 2. Modifying TPA-RFC-2 to mention office hours again, partly
    reverting commit
    tpo/tpa/wiki-replica@9c4d600a5616025d9b452bc19048959a99ea9997

 3. Trying to attend for a couple of weeks, see how it goes

# Deadline

Let's try next week, on November 27th.

If we don't have fun or forget that we even wanted to do this, revert
this in 2025.

# References

See also [TPA-RFC-34][] and the [discussion issue][].

 [discussion issue]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41882
