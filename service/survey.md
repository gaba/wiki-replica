Tor Project runs a self-hosted instance of
[LimeSurvey](https://www.limesurvey.org/) CE (community edition) to conduct
user research and collect feedback.

The URL for this service is https://survey.torproject.org/

The onionv3 address is
http://eh5esdnd6fkbkapfc6nuyvkjgbtnzq2is72lmpwbdbxepd2z7zbgzsqd.onion/

[[_TOC_]]

# Tutorial

## Create a new account

 1. Login to the [admin interface][] (see `tor-passwords` repo for credentials)
 2. Navigate to `Configuration` -> [User management][]
 3. Click the `Add user` button on the top left corner
 4. Fill in `Username`, `Full name` and `Email` fields
 5. If `Set password now?` is left at `No`, a welcome email will be sent to the
    email address
 6. Select the appropriate [roles] in the `Edit permissions` table:
   * For regular users who should be able to create and manage their own
   surveys, there is a role called 'Survey Creator' that have "Permission to create surveys (for which all permissions are automatically given) and view, update and delete surveys from other users". Otherwise you can select the checkboxes under the `Create` and `View/read` columns in the `Permission to create surveys` row.
   * For users that may want to edit or add themes, there is a role called 'Survey UXer' with permissions to create, edit or remove surveys as well as create or edit themes.
 7. Please remind the new user to draft a data retention policy for their survey and add an expiration date to the surveys they create.

[User management]: https://survey.torproject.org/index.php/userManagement/index
[roles]: https://survey.torproject.org/index.php/userRole/index

Note: we _don't_ want to use [user groups][] since they do not have the effects that we would expect them to have.

[user groups]: https://www.limesurvey.org/manual/Manage_user_groups

# How-to

## Upgrades

We don't use the paid [ComfortUpdate][] extension that is promoted and sold by
LimeSurvey.

Instead, we deploy from the latest stable zip-file release using Puppet.

The steps to upgrade LimeSurvey are:

 0. Review the LimeSurvey upstream [changelog](https://github.com/LimeSurvey/LimeSurvey/blob/master/docs/release_notes.txt)
 1. Login to `survey-01`, stop Puppet using `puppet agent --disable "pending LimeSurvey upgrade"`
 2. Open the [LimeSurvey latest stable release page][] and note the version
    number and sha256 checksum
 3. In the `tor-puppet` repository, edit `hiera/roles/survey.yaml` and update
    the version and checksum keys with above info
 4. Enable full maintenance mode

    sudo -u postgres psql -d limesurvey -c "UPDATE lime_settings_global SET stg_value='hard' WHERE stg_name='maintenancemode'"

 5. Run the puppet agent on `survey-01`: `puppet agent --enable && pat`: Puppet
    will unpack the new archive under `/srv/www/survey.torproject.org/${version}`,
    update the Apache vhost config and run the database update script
 7. Login to the [admin interface][] and validate the new version is running
 8. Disable maintenance mode:

    sudo -u postgres psql -d limesurvey -c "UPDATE lime_settings_global SET stg_value='off' WHERE stg_name='maintenancemode'"

Because LimeSurvey does not make available previous release zip-files, the old
code installation directory is kept on the server, along with previously
downloaded release archives. This is intentional, to make rolling back easier
in case of problems during an upgrade.

[ComfortUpdate]: https://community.limesurvey.org/comfort-update-extension/
[LimeSurvey latest stable release page]: https://account.limesurvey.org/stable-release
[admin interface]: https://survey.torproject.org/admin

## Pager playbook

<!-- information about common errors from the monitoring system and -->
<!-- how to deal with them. this should be easy to follow: think of -->
<!-- your future self, in a stressful situation, tired and hungry. -->

## Disaster recovery

In case of a disaster restoring both `/srv` and the PostgreSQL database on a
new server should be sufficient to get back up and running.

# Reference

## Installation

<!-- how to setup the service from scratch -->

## SLA

<!-- this describes an acceptable level of service for this service -->

## Design

This service runs on a standard Apache/PHP/PostgreSQL stack.

Self-hosting a LimeSurvey instance allows us to better safeguard user-submitted
data as well as allowing us to make it accessible through an onion service.

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution", it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

<!-- a good guide to "audit" an existing project's design: -->
<!-- https://bluesock.org/~willkg/blog/dev/auditing_projects.html -->

<!-- things to evaluate here:

 * services
 * storage (databases? plain text files? cloud/S3 storage?)
 * queues (e.g. email queues, job queues, schedulers)
 * interfaces (e.g. webserver, commandline)
 * authentication (e.g. SSH, LDAP?)
 * programming languages, frameworks, versions
 * dependent services (e.g. authenticates against LDAP, or requires
   git pushes) 
 * deployments: how is code for this deployed (see also Installation)

how is this thing built, basically? -->

## Issues

<!-- such projects are never over. add a pointer to well-known issues -->
<!-- and show how to report problems. usually a link to the -->
<!-- issue tracker. consider creating a new Label to regroup the -->
<!-- issues if using the general tracker. see also TPA-RFC-19. -->

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~Survey.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Survey

## Maintainer, users, and upstream

<!-- document who deployed and operates this service, who the users -->
<!-- are, who the upstreams are, if they are still active, -->
<!-- collaborative, how do we keep up to date, -->

## Monitoring and testing

<!-- describe how this service is monitored and how it can be tested -->
<!-- after major changes like IP address changes or upgrades. describe -->
<!-- CI, test suites, linting, how security issues and upgrades are -->
<!-- tracked -->

## Logs and metrics

<!-- where are the logs? how long are they kept? any PII? -->
<!-- what about performance metrics? same questions -->

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

<!-- references to upstream documentation, if relevant -->

# Discussion

<!-- the "discussion" section is where you put any longer conversation -->
<!-- about the project that you will not need in a casual -->
<!-- review. history of the project, why it was done the way it was -->
<!-- (as opposed to how), alternatives, and other proposals are -->
<!-- relevant here. -->

<!-- this at least partly overlaps with the TPA-RFC process (see -->
<!-- policy.md), but in general should defer to proposals when -->
<!-- available -->

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. --> 

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->
