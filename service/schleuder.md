# Schleuder

[[_TOC_]]

Schleuder is a gpg-enabled mailing list manager with
resending-capabilities. Subscribers can communicate encrypted (and
pseudonymously) among themselves, receive emails from non-subscribers
and send emails to non-subscribers via the list.

For more details see https://schleuder.org/schleuder/docs/index.html.

Schleuder runs on mta.chameleon (part of Tails infra). The version of Schleuder currently
installed is: 4.0.3

Note that Schleuder was considered for retirement but eventually migrated, see
[TPA-RFC-41](policy/tpa-rfc-41-schleuder-retirement) and
[TPA-RFC-71](policy/tpa-rfc-71-emergency-email-deployments-round-2).

## Using Schleuder

Schleuder has it's own gpg key, and also it's own keyring that you can use if you are subscribed to the list.

All command-emails need to be signed.

### Sending emails to people outside of the list

When using X-RESEND you need to add also the line X-LIST-NAME line to your email, and send it signed:

    X-LIST-NAME: listname@withtheemail.org
    X-RESEND: person@nogpgkey.org

You could also add their key to your schleuder mailing list, with

    X-LIST-NAME: listname@withtheemail.org
    X-ADD-KEY:
    [--- PGP armored block--]

And then do:

    X-LIST-NAME: listname@withtheemail.org
    X-RESEND-ENCRYPTED-ONLY: person@nogpgkey.org

### Getting the keys on a Schleuder list keyring

    X-LIST-NAME: listname@withtheemail.org
    X-LIST-KEYS

And then:

    X-LIST-NAME: listname@withtheemail.org
    X-GET-KEY: someone@important.org

## Administration of lists

There are two ways to administer schleuder lists: through the CLI interface of the schleuder API daemon (sysadmins only), or by sending PGP encrypted emails with the appropriate commands to `listname-request@withtheemail.org`.

### Pre-requisites

#### Daemon

Mailing lists are managed through schleuder-cli which needs schleuder-api-daemon running.

The daemon is configured to start automatically, but you can verify it's running using systemctl:

    sudo systemctl status schleuder-api-daemon

#### Permissions

The `schleuder-cli` program should be executed in the context of `root`.

#### PGP

For administration through the `listname-request` email interface, you will need the ability to encrypt and sign messages with PGP. This can be done through your email client, or with `gpg` on the command line with the armored block them copied into a plaintext email.

All email commands must be PGP encrypted with the public key of the mailing list in question. Please follow the instructions above for obtaining that mailing list's key.

### List creation

To create a list you add the list to [hiera](https://gitlab.tails.boum.org/tails/puppet-code/-/blob/production/hieradata/node/mta.chameleon.eyaml?ref_type=heads).

Puppet will tell schleuder to create the list gpg key together with the list. Please not that the created keys do not expire. For more information about how Schleuder creates keys you can check: https://0xacab.org/schleuder/schleuder/blob/master/lib/schleuder/list_builder.rb#L120

To export a list public key you can do the following:

    sudo schleuder-cli keys export secret-team@lists.torproject.org <list-key-fingerprint>

### List retirement

To delete a list, remove it from hiera and run:

    sudo schleuder-cli lists delete secret-team@lists.torproject.org

This will ask for confirmation before deleting the list and all its
data.

### Subscriptions management

#### CLI daemon

Subscription are managed with the subscriptions command.

To subscribe a new user to a list do:

    sudo schleuder-cli subscriptions new secret-team@lists.torproject.org person@torproject.org <fingerprint> /path/to/public.key

To list current list subscribers:

    sudo schleuder-cli subscriptions list secret-team@lists.torproject.org

To designate (or undesignate) a list admin:

    sudo schleuder-cli subscriptions set secret-team@lists.torproject.org person@torproject.org admin true

#### Email commands

Lists can also be administered via email commands sent to `listname-request@lists.torproject.org` (list name followed by `-request`). Available commands are described in the [Schleuder documentation for list-admins](https://schleuder.org/schleuder/docs/list-admins.html).

To subscribe a new user, you should first add their PGP key. To do this, send the following email to `listname-request@lists.torproject.org`, encrypted with the public key of the mailing list and signed with your own PGP key:

```
x-listname listname@lists.torproject.org
x-add-key
-----BEGIN PGP PUBLIC KEY BLOCK-----
-----END PGP PUBLIC KEY BLOCK-----
```

You should receive a confirmation email similar to the following that the key was successfully added:

```
This key was newly added:
0x1234567890ABCDEF1234567890ABCDEF12345678 user@domain.tld 1970-01-01 [expires: 2080-01-01]
```

After adding the key, you can subscribe the user by sending the following (signed and encrypted) email to `listname-request@lists.torproject.org`:

```
x-listname listname@lists.torproject.org
x-subscribe user@domain.tld 0x1234567890ABCDEF1234567890ABCDEF12345678
```

You should receive a confirmation email similar to the following:

```
user@domain.tld has been subscribed with these attributes:

Fingerprint: 1234567890ABCDEF1234567890ABCDEF12345678
Admin? false
Email-delivery enabled? true
```

### Other commands

All the other commands are available by typing:

    sudo schleuder-cli help

### Migrating lists

To migrate a schleuder list, go through the following steps:

  - export the public and secret keys from the list:
      - `gpg --homedir /var/lib/schleuder/lists/[DOMAIN]/[LIST]/ --armor --export > ~/list-pub.asc`
      - `gpg --homedir /var/lib/schleuder/lists/[DOMAIN]/[LIST]/ --armor --export-secret-keys > ~/list-sec.asc`
  - create the list on the target server, with yourself as admin
  - delete the list's secret key on the target server
  - copy list-pub.asc and list-sec.asc from the old server to the target server and import them in the list keyring
  - adjust the list fingerprint in the lists table in /var/lib/schleuder/db.sqlite
  - copy the subscriptions from the old server to the new
  - remove yourself as admin
  - change the mail transport for the list
  - remove the list from the old server
  - remove all copies of list-sec.asc (and possible list-pub.asc)

## References for sysadmins

### Known lists

The list of Schleuder lists can be found in [hiera](https://gitlab.tails.boum.org/tails/puppet-code/-/blob/production/hieradata/node/mta.chameleon.eyaml?ref_type=heads)

### Threat model

#### ci

Used to organize around the Tails CI.

No sensitive data.

Interruption not so problematic.

If hosted on lizard, interruption is almost not a problem at all: there won't be anything
to report about or discuss if lizard is down.

Requirements:
    Confidentiality: low
    Availability: low
    Integrity: low

→ puscii

#### rm

- Used to organize around the Tails release management.
- advance notice for embargoed (tor) security issues and upcoming Firefox chemspill releases
- Jenkins failure/recovery notifications for release branches (might contain
  some secrets about our CI infra occasionally)

Interruption effect? Probably none: small set of members who also have direct communication channels and often use them instead of the mailing list

Requirements:
    Confidentiality: medium--high
    Availability: low
    Integrity: low

→ Tails infra

#### fundraising

- list of donors
- discussion with past & potential sponsors
- daily rate of each worker
- internal view of grants budget

Requirements:
    Confidentiality: medium--high
    Availability: medium--high
    Integrity: medium--high

→ puscii

#### accounting

- contributors' private/identifying personal info
- contracts
- accounting
- expenses reimbursement
- management and HR stuff
- administrativa and fiscal info
- discussion with current sponsors

Requirements:
    Confidentiality: high
    Availability: medium--high
    Integrity: high

→ Tails infra

#### press

Public facing address to talk to the press and organize the press team.

No sensitive data.

Interruption can be problematic in case of fire to communicate with the outside.

Requirements:
    Confidentiality: medium
    Availability: medium--high (high in case of fire)
    Integrity: medium--high

→ puscii

#### bugs

Public facing address to talk to the users and organize the team.

Contains sensitive data (whisperback reports and probably more).

Interruption can be problematic in case of fire to communicate with the outside ?

Requirements:
    Confidentiality: high
    Availability: medium--high (high in case of fire)
    Integrity: high

→ Tails infra but availability issue ⇒ needs mitigation

#### tails@

- internal discussions between Tails "wizards"
- non-technical decision making e.g. process
- validating new members for other teams
- sponsorship requests

Requirements:
    Confidentiality: medium--high
    Availability: medium--high (very high in case of fire)
    Integrity: high

→ puscii but integrity issue ⇒ needs mitigation (revocation procedure?)

#### summit

- internal community discussions

Requirements:
    Confidentiality: medium
    Availability: medium
    Integrity: low

→ puscii

#### sysadmins

- monitoring alerts
- all kinds of email sent to root e.g. cron
- occasionally some secret that could give access to our infra?

Requirements:
    Confidentiality: high (depending on the occasional secret, else medium)
    Availability: medium--high (in case of fire, there are other means
    for sysadmins to reach each other, and for other Tails people who can/should do
    something about it to reach them; outsiders rarely contact Tails sysadmins
    for sysadmin stuff anyway)
    Integrity: high

→ Tails infra

#### mirrors

- discussion with mirror operators
- enabling/disabling mirrors (mostly public info)

Requirements:
    Confidentiality: low--medium
    Availability: low--medium (medium in case of fire) <- do we have backup contacts?
                              Yes, all the contact info for mirror operators
                              is in a public Git repo and they are technically skilled
                              people who'll find another way to reach us
                              => I would say low--medium even in case of fire.
    Integrity: medium (impersonating this list can lead mirror operators to misconfigure
                      their mirror => DoS i.e. users cannot download Tails; although
                      that same attack would probably work on many mirror operators
                      even without signing the email…)

→ puscii

### Basic threats

compromise of schleuder list -> confidentiality & integrity

schleuder list down -> availability


### Basic Scenarios

#### 1. List confidentiality compromised due to compromised member/admin mailbox + pgp key

This can happen unnoticed

#### 2. List integrity compromised due to compromised member/admin mailbox + pgp key

This will be noticed as the resend notifies the list

#### 3. List confidentiality compromised due to server compromise

This can happen unnoticed

#### 4. List integrity compromised due to compromised member/admin mailbox + pgp key

This can happen unnoticed

#### 5. List availability down because of misconfiguration

#### 6. List availability down because of server down
