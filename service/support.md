# Support Portal

Tor Support Portal is a static site based on [Lektor](https://getlektor.com). The code of the website is located at [Support Portal repository](https://github.com/torproject/support) and you can submit pull requests via github.

The Support Portal is hosted at several computers for redundancy, and these computers are together called "the www rotation". Please check the [static sites](https://help.torproject.org/tsa/doc/static-sites/) help page for more info.

The support portal has a staging environment: [support.staging.torproject.net/support/staging/](https://support.staging.torproject.net/)

And a production environment: [support.torproject.org](https://support.torproject.org)

# How to update the content

To update the content you need to:

- Install lektor and the lektor-i18n plugin
- Clone our repository
- Make your changes and verify they look OK on your local install
- Submit a pull request at our repository

1. Install lektor: <https://www.getlektor.com/downloads/>

1. Clone the repo: <https://github.com/torproject/support/>

1. The translations are imported by GitLab when building the page, but if you want to test them, clone the correct branch of the translations repo into the ./i18n/ folder:

```
git clone https://gitlab.torproject.org/tpo/translation.git i18n
cd i18n
git checkout support-portal
```

   TODO: the above documentation needs to be updated to follow the
   Jenkins retirement.

1. Install the i18n plugin:

```
lektor plugins add lektor-i18n
```

## Content and Translations structure

The support portal takes the files at the [the /content folder](https://github.com/torproject/support/tree/master/content) and creates html files with them. The website source language is English.

Inside the content folder, each subfolder represents a support topic. In this case the contents.lr is where the topic title is defined and the control key that decides the order of the topic within all the questions list.

### Topics

For each topic folder there will be a number of subfolders representing a question each. For each question there is a .lr file  and in your local install there will be locale files in the format `contents+<locale>.lr`. **Dont edit the `contents+<locale>.lr` files, only the `contents.lr` file**. The `contents+<locale>.lr`, for example `contents+es.lr`, are generated from the translation files automatically.

So for example, all the questions that appear at <https://support.torproject.org/connecting/> can be seen at <https://github.com/torproject/support/tree/master/content/connecting>

### Questions

Example:
<https://github.com/torproject/support/blob/master/content/connecting/connecting-2/contents.lr> that becomes <https://support.torproject.org/connecting/connecting-2/>

Inside a contents file you will find question title and description in the format:

```
_model: question
---
title: Our website is blocked by a censor. Can Tor Browser help users access our website?
---
description:

Tor Browser can certainly help people access your website in places where it is blocked.
Most of the time, simply downloading the <a href="https://www.torproject.org/download/download-easy.html.en">Tor Browser</a> and then using it to navigate to the blocked site will allow access.
In places where there is heavy censorship we have a number of censorship circumvention options available, including <a href="https://www.torproject.org/docs/pluggable-transports.html.en">pluggable transports</a>.

For more information, please see the <a href="https://tb-manual.torproject.org/en-US/">Tor Browser User Manual</a> section on <a href="https://tb-manual.torproject.org/en-US/circumvention.html">censorship</a>.
```

When creating a document refer to the [Writing content guide from the
web team](https://gitlab.torproject.org/tpo/web/tpo/-/wikis/Writing-the-content).

Then you can make changes to the contents.lr files, and then

```
lektor build
lektor server
```

You will be able to see your changes in your local server at <http://127.0.0.1:5000/>

### Update translations

Similarly, if you want to get the last translations, you do:

```

cd i18n
git reset --hard HEAD # this is because lektor changes the .po files and you will get a merge conflict otherwise
git pull
```

## Add a new language to the Support portal

This is usually done by emmapeel, but it is documented here just in case:

To add a new language, it should appear first here:

<https://gitlab.torproject.org/tpo/translation/-/tree/support-portal_completed?ref_type=heads>

You will need to edit this files:

```
- databags/alternatives.ini
- configs/i18n.ini 
- portal.lektorproject
```

and then, create the files:

```
export lang=bn
cp databags/menu+en.ini databags/menu+$lang\.ini
cp databags/topics+en.ini databags/topics+$lang\.ini
```
