# Tails Sysadmins role description

:warning: This page became outdated with the [Tails/Tor merge
process](/tpo/tpa/team/-/wikis/policy/tpa-73-tails-infra-merge-roadmap). Right
now, TPA is operating in a hybrid way and this role description should be
updated as part of tpo/tpa/team#41943.

[[_TOC_]]

## Goals

The Tails system administrators set up and maintain the infrastructure
that supports the development and operations of Tails. We aim at
making the life of Tails contributors easier, and to improve the quality of
the Tails releases.

## Main responsibilities

These are the main responsibilities of Tails Sysadmins:

* Deal with hardware purchase, upgrades and failures.

* Install and upgrade operating systems and [[services|tails/services]].

* Organize on [[shifts|tails/processes/shifts]].

* Discuss, support and implement requests from teams.

* Have root access to all hosts.

## Principles

When developing for and administering the Tails infrastructure, Sysadmins aim to:

* Use Free Software, as defined by the [Debian Free Software
  Guidelines](https://www.debian.org/social_contract#guidelines).
  The firmware our systems might need are the only exception to
  this rule.

* Treat system administration like a (free) software development project. This
  is why we try to publish as much as possible of our systems configuration,
  and to manage our whole infrastructure with configuration management tools.
  That is, without needing to log into hosts:

  * We want to enable people to participate without needing an account
    on the Tails servers.

  * We want to review the changes that are applied to our systems.

  * We want to be able to easily reproduce our systems via
    automatic deployment.

  * We want to share knowledge with other people.

## Communication within Tails

In order to maintain good communication with the rest of Tails, Sysadmins should:

* Report once a month in assembly@ about activities.

* Maintain an up-to-date public [[shift calendar|https://nc.torproject.net/apps/calendar/p/WsXFeLMw62anndqc]].

* Read e-mail at least twice a week.

* Triage and garden [issues in the `tails-sysadmin` GitLab project](https://gitlab.torproject.org/tpo/tpa/tails-sysadmin/-/issues).

## External relations

These are the main relations Sysadmins have with the outside world:

* Serve as an interface between Tails and hosting providers.

* Relate to (server-side software) upstream according to the broader [[Tails principles|https://tails.net/contribute/relationship_with_upstream]].

* Communicate with mirror operators.

## Necessary and useful skills and competences

The main tools used to manage the Tails infrastructure are:

* [Debian](https://www.debian.org/) GNU/Linux; in the vast majority of
  cases, we run the current stable release.

* [Puppet](https://www.puppet.com/),
  a configuration management system.

* [Git](http://git-scm.com/) to host and deploy configuration,
  including our [[Puppet code|https://tails.net/contribute/git#puppet]]

Other useful skills:

* Patience and diligence.

* Ability to self-manage (by oneself and within the team), prioritize and plan.

## Contact

In order to get in touch with Tails sysadmins, you can:

* Create an issue in the [project](https://gitlab.torproject.org/tpo/tpa/tails-sysadmin).

* Ping all sysadmins anywhere in our [GitLab](https://gitlab.tails.boum.org/desc="GitLab") by mentioning the `@sysadmin-team` group.

* See if one of us is on shift in [[one of our chat rooms|https://tails.net/about/contact#chat]].

* Send an e-mail to [[the sysadmin's mailing list|https://tails.net/about/contact#tails-sysadmins]].
