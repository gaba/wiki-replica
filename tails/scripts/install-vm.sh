#!/bin/bash

HOSTNAME=""
DOMAIN=$(/usr/bin/hostname)
GATEWAY=$(/usr/bin/ip -4 addr show dev virbr0 | /usr/bin/grep -oP '(?<=inet\s)\d+(\.\d+){3}')
DNS=$GATEWAY
NETMASK="255.255.255.0"
DISKSIZE="10"
VCPU="4"
RAM="4000"
IP=""
HELPME=""
PASS=$(pwgen -ns 16 1)
SALT=$(pwgen -ns 8 1)
CRYPT=$(mkpasswd -m sha-512 -S $SALT $PASS)

while getopts n:d:i:v:r:h option
do
  case "${option}"
  in
    n) HOSTNAME=${OPTARG};;
    d) DISKSIZE=${OPTARG};;
    v) VCPU=${OPTARG};;
    i) IP=${OPTARG};;
    r) RAM=${OPTARG};;
    h) HELPME="help";;
  esac
done

usage () {
  echo "Usage: install-vm.sh [-d disksize] [-v vcpu] [-r ram] -n hostname -i ip"
  exit $1
}

if [[ ! -z "$HELPME" ]]
then
  usage 0
fi

if [ -z "$HOSTNAME" ]
then
  echo "Missing hostname!"
  usage 1
fi

if [ -z "$IP" ]
then
  echo "Missing IP address!"
  usage 1
fi

echo "$VCPU cpus with $DISKSIZE GB disk and $RAM RAM"
echo "Your password is: $PASS"

cat > preseed.cfg << EOF
# Localisation
d-i debian-installer/locale string en_US.UTF-8
d-i keyboard-configuration/xkb-keymap select us

# Time
d-i time/zone select UTC
# HW clock set to UTC?
d-i clock-setup/utc boolean true
# Use NTP during install
d-i clock-setup/ntp boolean true

# Network
# Networking will need to be set-up to download the preseed file.  Although
# there are hacks (see: https://help.ubuntu.com/lts/installation-guide/s390x/apbs04.html#preseed-network)
# to work around this, it is easier just to set them on the kernel command
# line.

d-i netcfg/disable_autoconfig boolean true

d-i netcfg/get_hostname string $HOSTNAME
d-i netcfg/get_domain string $DOMAIN

d-i netcfg/get_ipaddress string $IP
d-i netcfg/get_netmask string $NETMASK
d-i netcfg/get_gateway string $GATEWAY
d-i netcfg/get_nameservers string $DNS
d-i netcfg/confirm_static boolean true

d-i netcfg/hostname string $HOSTNAME

# Mirror
d-i mirror/country string US
d-i mirror/http/mirror select ftp.us.debian.org
d-i mirror/http/proxy string

# Users
# Skip creation of a normal user account (Salt will do this later for us)
d-i passwd/user-fullname string toto
d-i passwd/username string toto
d-i passwd/user-password-crypted password $CRYPT
# Standard root password
d-i passwd/root-password-crypted password $CRYPT

# Partitioning
# VMs: auto partition whole disk, no LVM
d-i partman-auto/method string regular
# Do not complain there is no swap
d-i partman-basicfilesystems/no_swap boolean false
# Expert partitioning, 1000MB minimum, -1 maximum (all available space)
d-i partman-auto/expert_recipe string singleroot :: 1000 50 -1 ext4 \
   \$primary{ } \$bootable{ } method{ format } \
   format{ } use_filesystem{ } filesystem{ ext4 } \
   mountpoint{ / } \
   .
d-i partman-auto/choose_recipe select singleroot
# This makes partman automatically partition without confirmation, provided
# that you told it what to do using one of the methods above.
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

# Base system install
d-i base-installer/kernel/image select linux-image-amd64

# Package selection
# No 'task' selections
tasksel tasksel/first multiselect
# Auto-install ssh and puppet, so the system comes up ready to be managed
d-i pkgsel/include string openssh-server puppet ruby-gpgme
# Partake in the popularity contest
popularity-contest popularity-contest/participate boolean false

# Bootloader
# This is fairly safe to set, it makes grub install automatically to the MBR
# if no other operating system is detected on the machine.
d-i grub-installer/only_debian boolean true
# Due notably to potential USB sticks, the location of the MBR can not be
# determined safely in general, so this needs to be specified:
d-i grub-installer/bootdev string /dev/vda

# Avoid that last message about the install being complete.
d-i finish-install/reboot_in_progress note

d-i preseed/late_command string \
echo "192.168.122.32  puppet.lizard   puppet" >> /target/etc/hosts; \
echo "[main]" > /target/etc/puppet/puppet.conf; \
echo "  certname=$HOSTNAME.$DOMAIN" >> /target/etc/puppet/puppet.conf; \
echo "  hostprivkey = \\\$privatekeydir/\\\$certname.pem { mode = 640 }" >> /target/etc/puppet/puppet.conf; \
echo "  privatekeydir = \\\$ssldir/private_keys { group = service }" >> /target/etc/puppet/puppet.conf; \
echo "  ssldir = /var/lib/puppet/ssl" >> /target/etc/puppet/puppet.conf; \
echo "  server=puppet.lizard" >> /target/etc/puppet/puppet.conf; \
echo "[agent]" >> /target/etc/puppet/puppet.conf; \
echo "  environment = production" >> /target/etc/puppet/puppet.conf;
EOF

virt-install --name $HOSTNAME --vcpus $VCPU --memory $RAM \
  --virt-type kvm --hvm \
  --cpu host-passthrough --memorybacking hugepages=yes \
  --numatune memory.mode=strict,memory.placement=auto \
  --iothreads $VCPU \
  --initrd-inject=./preseed.cfg \
  --network network=default,filterref.filter=clean-traffic \
  --xml xpath.create=./devices/interface/filterref/parameter \
  --xml xpath.set=./devices/interface/filterref/parameter/@name=IP \
  --xml xpath.set=./devices/interface/filterref/parameter/@value=$IP \
  --location http://deb.debian.org/debian/dists/bullseye/main/installer-amd64/ \
  --os-variant debian10 --graphics none --console pty,target_type=serial \
  --extra-args "auto-install/enable=true ip=$IP::$GATEWAY:$NETMASK:$HOSTNAME:eth0:off console=ttyS0" \
  --disk size=$DISKSIZE,cache=directsync,io=native,discard=unmap \
  --autostart
