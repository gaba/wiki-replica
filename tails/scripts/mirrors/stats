#!/usr/bin/python3

import json
import subprocess


def count_with_email_address(mirrors):
    """Return number of mirrors in MIRRORS that have an email address"""
    return len([mirror for mirror in mirrors if 'email' in mirror])


if __name__ == "__main__":
    with open('mirrors.json', 'r', encoding='utf-8') as f:
        mirrors = [
            mirror
            for mirror in json.loads(f.read())['mirrors']
            if mirror['weight'] > 0
        ]

    total = len(mirrors)
    with_address = count_with_email_address(mirrors)
    without_address = total - with_address

    print(f"Total: {total} mirrors enabled, among which:")
    print()
    print(f" - {with_address} with an email address")
    print(f"   {without_address} without an email address")
    print()
    print(" - GeoIP:")
    geoip_output = subprocess.check_output('./bin/geoip',  # nosec
                                           universal_newlines=True)
    print("   " + "\n   ".join(geoip_output.split("\n")))
