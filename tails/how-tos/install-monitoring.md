# Install a new Icinga 2 node

:warning: This process will change with tpo/tpa/team#41946 and this page should
be updated when that happens.

[[_TOC_]]

When you have deployed a new node with our puppetmaster, the system
already has a basic Icinga2 service installed and managed, with a basic
mostly disabled configuration.

In order to activate the monitoring of the your new node, you still have
a few steps to go through.

## Configure your node in Puppet

Most of the time the node you'll installed will just be a simple agent
that will report somewhere.

To configure it, add

    class { 'tails::profile::monitoragent': }

to your node definition in Puppet.

Also add your node to `monitoring::agents` in puppet-code:hieradata/common.yaml
At the bare minimum, you should add an address and vars.os for this node.

At this point, you can push your changes and run puppet agent -t on both
ecours and your new node. This should get you 95% of the way.

## Certificates

We still need icinga2 on ecours to sign the certificate of our new node.

In the new node, use the following command to see its certificate's
fingerprint:

    openssl x509 -noout -fingerprint -sha256 -in \
        "/var/lib/icinga2/certs/$(hostname --fqdn).crt"

Then log in to ecours, and run:

    sudo -u nagios icinga2 ca list

You should see an entry for your new node. Check the fingerprint and, finally, run:

    sudo -u nagios icinga2 ca sign <fingerprint>

Now you should have monitoring of your new node up and running.
