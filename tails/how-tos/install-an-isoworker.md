# Installing a Jenkins isoworker

    :warning: This process will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

1. Follow the instructions for installing a VM.

2. Create two XMPP accounts on https://jabber.systemli.org/register_web

3. Configure the accounts on your local client, make them friends, and generate an OTR key for the second account.

4. In puppet-hiera-node, create an eyaml file with tails::jenkins::slave::iso_tester::pidgin_config data, using the account and OTR key data created in steps 2 and 3.

5. Also in puppet-hiera-node, make sure you have the firewalling rules copied from one of the other isoworkers in your `$FQDN.yaml` file.

6. In puppet-code, update the hieradata/node submodule and in manifests/nodes.pp add ``include tails::profile::jenkins::isoworker`` to the node definition.

7. On the VM, run puppet agent -t once, this should generate an SSH key for user root.

8. Log in to gitlab.tails.boum.org as root and go to: https://gitlab.tails.boum.org/admin/users/role-jenkins-isotester . Click "Impersonate", go to "Edit profile" -> "SSH Keys", and add the public SSH key generated in step 6 (/root/.ssh/id_rsa.pub on the new node) to the to user's SSH keys. Make sure it never expires.

9. Go to https://jenkins.tails.net/computer and add the new node. If the node is running on our fastest hardware, make sure to set the Preference Score accordingly.

10. Under https://jenkins.tails.net/computer/(built-in)/configure , increase the 'Number of executors' by one.

11. Add the new node in our jenkins-jobs repository. To see where we hardcode the list of slaves: `git grep isoworker`
 
12. On the new node, run puppet agent -t several times and reboot. After this, you should have a functional isoworker.
