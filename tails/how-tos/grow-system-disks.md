# Growing a VM's system disk

:warning: This process will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

These are instructions for growing the size of a VM's system disk. For these
disks, there are 2 levels of LVM:

1. A logical volume is defined in lizard as /dev/lizard/[VM]-system and maps to
   /dev/vda inside the VM.
2. The /dev/vda is partitioned inside the VM and /dev/vda2 is made an LVM
   physical volume. That physical volume is a part of the "vg1" volume group
   and a "root" logical volume is created in that group, providing /dev/vg1/root.

**Attention:** these instructions do not apply to data disks, as their
partitioning scheme is different from system disks.

## Instructions

Please, double check these instructions before running them to make sure the
partitioning scheme makes sense for the case.

Resize the system disk in the host:

    VM=www
    AMOUNT=2G
    sudo virsh shutdown ${VM}
    # wait for VM to shutdown, then:
    sudo lvresize -L+${AMOUNT} /dev/lizard/${VM}-system
    sudo virsh start ${VM}

SSH into the VM:

    ssh ${VM}.lizard

Resize the block device and LVM volumes from inside the VM:

    sudo parted /dev/vda resizepart "2 -1s"
    sudo pvresize /dev/vda2
    sudo lvresize -l+100%FREE /dev/vg1/root
    sudo resize2fs /dev/vg1/root

This should be enough!
