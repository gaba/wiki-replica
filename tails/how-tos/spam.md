# SPAM training guide

Schleuder messages are copied to the `vmail` system user mailbox and
automatically deleted after 3 days, so we have a chance to do manual training
of SPAM.

This happens in both mail servers:

- `mail.lizard`: hosts old `@boum.org` Schleuder lists and redirects mail sent
  to them to the new `@tails.net` lists.
- `mta.chameleon`: hosts new `@tails.net` Schleuder lists.

To manually train our antispam, SSH into one of the servers above and then:

```
sudo -u vmail mutt
```

Shortcuts:

- `s` for SPAM
- `h` for HAM (no-SPAM)
- `d` to delete

**Important:** If you're in `mta.tails.net`, do not train mail from
`@boum.org`, but instead just delete them because we don't want to teach the
filter to think that encrypted mail is spam.
