# RM Q&A for Sysadmins

[[_TOC_]]

This is the output of a session between Zen-Fu and Anonym where they quickly
went through the Release Process documentation
(https://tails.net/contribute/release_process/) and tried to pin point
which parts of the Infra are used during the release process.

## Q: Do you use Gitolite repos at git.tails.net?

A: Not anymore!

## Q: How are APT repos related to Git repos?

A: Each branch and tag of tails.git generates a correspondent APT repo (even
   feature-branches).

## Q: What are APT overlays?

A: They are APT repos made from feature branches. If there are files in
   config/APT_overlays.d named according to branches, those APT repos are included
   when Tails is built. Then, right before a release, we merge these feature
   branch APT suites into the "base" APT suite used for the release (stable or
   testing) by bin/merge-APT-overlays.

   Example: applying patches from Tor --> create a branch `feature/new-tor` -->
   Push repo --> APT repo is created --> touch file in config/APT_overlays.d -->
   gets merged. If instead we merged into stable we wouldn't be able to revert.
   It's like a soft merge.

## Q: What are the base APT repos of Tails?

A: Stable (base, for releases), testing (when we freeze devel), development.

## Q: What "freeze snapshot" does?

A: It's local, sets URLs from where to fetch.

## Q: How to import tor browser?

A: There's a git-annex repo for it, which pushes to tor browser archive.

## Q: What kind of APT snapshots we have?

A: Time-based snapshots include everything on a repo on a certain moment (based on
   timestamps); and tagged snapshots (related to Git tags) contain exactly the
   packages included in a release.

## Q: Who actually builds images?

A: Jenkins, the RM, and trusted reproducers.

## Q: How's the website related to all this?

A: It has to announce new releases.

## Q: How are images distributed?

A: From jenkins, they go to rsync server which will seed mirrors.

## Q: Which IUKs do we maintain at a certain point in time?

A: Only IUKs from all past versions (under the same major release) to the
   latest version. When there's a new Debian, older IUKs are deleted.

## Q: Where are torrent files generated?

A: RM's system.

## Q: Does Schleuder play a role somewhere?

A: Yes, it's used to send e-mail to manual testers.

## Q: When should Sysadmins be present to support the release process?

A: Generally, the most intensive day is the day before the release, but RMs might do
   some work the days before. Check frequently with them to see if this eventually
   changes.

## Q: How do we know who'll be RM for a specific release?

A: https://tails.net/contribute/calendar/
