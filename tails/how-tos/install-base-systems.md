# Install new systems

:warning: This process will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

This note covers the installation of a new system that is **not** a
VM hosted on one of our physical machines.

* Install an OS on the new system.

* If this system needs some trustworthy connection to lizard or one of
  our other system (which is the case e.g. when the new system is
  meant to use lizard's puppetmaster), follow the VPN documentation:
  [[tails/services/vpn]].

* Follow [[tails/how-tos/install-a-vm]] starting from point 8.
  - Skip what is related to VM management.
  - When declaring the node in our manifest, the ecours.tails.net
    one can be a good source of inspiration, e.g for the
    firewall part.

* Setup what is necessary to boot the host if its disk is encrypted:
  check that its manifest installs dropbear, put the right `ip=` kernel
  boot option and add the necessary ssh keys to
  `/etc/initramfs-tools/root/.ssh/authorized_keys`.

* Take care also to update this documentation, e.g if the system **does
  not** use lizard's puppetmaster.

* Set up monitoring. Follow the [[monitoring installation
  notes|tails/how-tos/install-monitoring]], paying attention that:
  - Traffic on the VPN between the new host Icinga2 agent and ecours
    Icinga2 master (port 5665) must be whitelisted in their respective
    firewalls.

Have a look at ecours.tails.net node manifest and hiera data to look how
such a host monitoring is configured.

* Set up backups. Assuming you don't use or have access to LVM on this
  machine, we'll simply backup the filesystem, rather than using snapshots.
  Add a line in the new machine's section in the manifests/nodes.pp file.
  For example:

      tails::borgbackup::fs { 'my_new_machine': excludes => [ 'proc','dev','tmp','sys' ], }

  Where `my_new_machine` is the name of your new machine. If you expect
  significant amounts of rapidly changing data that does not need to be
  backed up, consider adding extra excludes.

  Now, generate a passwordless SSH key for root on the new machine and
  add the public key with `ssh_authorized_key` to masterless_manifests/stone.pp, making sure it
  provides access to user borg with the
  `command="borg serve --append-only"` restriction. Apply the new
  manifest on stone and then ssh from your new machine to stone to
  verify the fingerprint.

  After this, follow the instructions in [[tails/services/backups]]
  concerning new backups.
