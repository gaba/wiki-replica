This directory contains some scripts that have become obsolete or will become
soon. To see them, you need to clone this wiki's repository and look into this
directory.

Other pages:

- [[mirrors|tails/scripts/mirrors]]
