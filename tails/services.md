# Services managed by Tails Sysadmins

:warning: The documentation below is reasonably up-to-date, but the services
described in this page have not yet been handled by the [Tails/Tor merge
process](/tpo/tpa/team/-/wikis/policy/tpa-rfc-73-tails-infra-merge-roadmap).
Their descriptions should be updated as each service is merged, migrated,
retired or kept.

[[_TOC_]]

Below, importance level is evaluated based on:

* users' needs: e.g. if the APT repository is down, then the
  _Additional Software_ feature is broken;

* developers' needs: e.g. if the ISO build fails, then developers
  cannot work;

* the release process' needs: we want to be able to do an emergency
  release at any time when critical security issues are published.
  Note that in order to release Tails, one needs to first build Tails,
  so any service that's needed to build Tails is also needed to release Tails.

## APT repositories

- [general documentation](https://tails.net/contribute/APT_repository/)

### Custom APT repository

* purpose: host Tails-specific Debian packages

* [documentation](https://tails.net/contribute/APT_repository/custom)

* access: anyone can read, Tails core developers can write

* tools: [reprepro](https://tracker.debian.org/pkg/reprepro)

* configuration:

  - [`tails::profile::reprepro::custom` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/reprepro/custom.pp)

  - signing keys are managed with the `tails_secrets_apt` Puppet module

* importance: critical (needed by users, and to build & release Tails)

### Time-based snapshots of APT repositories

* purpose: host full snapshots of the upstream APT repositories we
  need, which provides the freezable APT repositories feature needed
  by the Tails development and QA processes

* [documentation](https://tails.net/contribute/APT_repository/time-based_snapshots)

* access: anyone can read, release managers have write access

* tools: [reprepro](https://tracker.debian.org/pkg/reprepro)

* configuration:

  - [`tails::profile::reprepro::snapshots::time_based` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/reprepro/snapshots/time_based.pp)

  - signing keys are managed with the `tails_secrets_apt` Puppet module

* importance: critical (needed to build Tails)

### Tagged snapshots of APT repositories

* purpose: host partial snapshots of the upstream APT repositories we
  need, for historical purposes and compliance with some licenses

* [documentation](https://tails.net/contribute/APT_repository/tagged_snapshots)

* access: anyone can read, release managers can create and publish new
  snapshots

* tools: [reprepro](https://tracker.debian.org/pkg/reprepro)

* configuration:

  - [`tails::profile::reprepro::snapshots::tagged` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/reprepro/snapshots/tagged.pp)

  - signing keys are managed with the `tails_secrets_apt` Puppet module

* importance: critical (needed by users and to release Tails)

## Bitcoind

* purpose: handle the Tails Bitcoin wallet

* access: Tails core developers only

* tools: bitcoind

* configuration:
  [`bitcoind` class](https://gitlab.tails.boum.org/tails/puppet-bitcoind/-/blob/master/manifests/init.pp)
* Vcs-Git: [bitcoin](https://gitlab.tails.boum.org/tails/bitcoin) and [libunivalue](https://gitlab.tails.boum.org/tails/libunivalue)

* importance: medium

* To save disk space: as the `bitcoin@bitcoin.lizard` user, run
  `bitcoin-cli getblockcount` to get the ID of the last block,
  then run `bitcoin-cli pruneblockchain XYZ`, with `XYZ` being
  a Unix timestamp that's at least 5 months in the past.

## BitTorrent

* purpose: seed the new ISO image when preparing a release

* [documentation](https://tails.net/contribute/release_process)

* access: anyone can read, Tails core developers can write

* tools: [transmission-daemon](https://tracker.debian.org/pkg/transmission-daemon)

* configuration: done by hand ([#6926](https://gitlab.tails.boum.org/tails/tails/-/issues/6926))

* importance: low

## DNS

* purpose: authoritative nameserver for the `tails.net` and
  `amnesia.boum.org` zones

* [documentation](tails/services/dns)

* access:

  - anyone can query this nameserver

  - members of the mirrors team control some of the content of the
    `dl.amnesia.boum.org` sub-zone

  - Tails sysadmins can edit the zones with `pdnsutil edit-zone`

* tools: [pdns](https://tracker.debian.org/pkg/pdns) with its MySQL backend

* configuration:

  - [`tails::profile::dns::*` resources](https://gitlab.tails.boum.org/tails/puppet-tails/-/tree/master/manifests/profile/dns)

  - [`powerdns` Puppet module](https://github.com/sensson/puppet-powerdns)

* importance: critical (most of our other services are not available
  if this one is not working)

## GitLab

* purpose:

  - host Tails issues

  - host most Tails [Git repositories](https://tails.net/contribute/git)

* access: public + some data with more restricted access

* operations documentation:

  - [[gitlab|tails/services/gitlab]]
  - [[gitlab-runners|tails/services/gitlab-runners]]

* end-user documentation: [GitLab](https://contribute/working_together/GitLab)

* configuration:

  - immerda hosts our GitLab instance using [this Puppet
    code](https://code.immerda.ch/immerda/ibox/puppet-modules/-/blob/master/ib_gitlab/manifests/instance.pp).

  - We don't have shell access.

  - Tails system administrators have administrator credentials inside GitLab.

  - Groups, projects, and access control:

     - [high-level documentation](https://tails.net/working_together/GitLab#access-control)

     - configuration: [tails/gitlab-config](https://gitlab.tails.boum.org/tails/gitlab-config)

* importance: critical (needed to release Tails)

* Tails system administrators administrate this GitLab instance.

## Gitolite

* purpose:

  - host Git repositories used by the puppetmaster and other services

  - host mirrors of various Git repositories needed on lizard,
    and whose canonical copy lives on GitLab

* access: Tails core developers only

* tools: [gitolite3](https://tracker.debian.org/pkg/gitolite3)

* configuration:
  [`tails::gitolite` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/gitolite.pp)

* importance: high (needed to release Tails)

## git-annex

* purpose: host the full history of Tails released images and Tor
  Browser tarballs

* access: Tails core developers only

* tools: [git-annex](https://tracker.debian.org/pkg/git-annex)

* configuration:

  - [`tails::profile::git_annex` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/git_annex.pp)

  - [`tails::profile::gitolite` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/gitolite.pp)

  - [`tails::profile::git_annex::mirror` defined resource](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/git_annex/mirror.pp)

* importance: high (needed to release Tails)

## Icinga2

* purpose: Monitor Tails online services and systems.

* access: only Tails core developers can read-only the Icingaweb2 interface,
  sysadmins are RW and receive notifications by email.

* tools: [Icinga2](https://tracker.debian.org/pkg/icinga2), [icingaweb2](https://tracker.debian.org/pkg/icingaweb2)

* configuration: not documented

* documentation: currently none

* importance: critical (needed to ensure that other, critical services are working)

## Jenkins

* purpose: continuous integration, e.g. build Tails ISO images from
  source and run test suites

* access: only Tails core developers can see the Jenkins web interface
  ([#6270](https://gitlab.tails.boum.org/tails/tails/-/issues/6270)); anyone can [download the built products](https://tails.net/contribute/how/testing)

* tools: [Jenkins](https://tracker.debian.org/pkg/jenkins), [jenkins-job-builder](https://tracker.debian.org/pkg/jenkins-job-builder)

* design and implementation documentation:

  - [[jenkins|tails/services/jenkins]]
  - [[automated-builds-in-jenkins|tails/services/jenkins/automated-builds-in-jenkins]]
  - [[automated-tests-in-jenkins|tails/services/jenkins/automated-tests-in-jenkins]]

* importance: critical (as a key component of our development process,
  needed to build IUKs during a Tails release)

## LimeSurvey

* purpose: user surveys, mainly for UX purposes

* [documentation](tails/services/limesurvey)

* access: sysadmins and UX members have shell access, as well as admin access to the web interface

* tools: limesurvey

* configuration:
  [`tails::profile::limesurvey`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/limesurvey.pp)

* importance: low to medium

## Mail

* purpose: handle incoming and outgoing email for some of our
  [[Schleuder lists|tails/services#schleuder]]

* documentation:

  - [[schleuder-lists-threat-model|tails/services/schleuder-lists-threat-model]]

* access: public MTA's listening on `mail.tails.net` and `mta.tails.net`

* tools: [postfix](https://tracker.debian.org/pkg/postfix), [rspamd](https://tracker.debian.org/pkg/rspamd)

* configuration:
  [`tails::profile::mta`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/mta.pp),
  [`tails::profile::rspamd`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/rspamd.pp),
  and
  [`tails::profile::mtasts`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/mtasts.pp)
  classes

* importance: high (at least because WhisperBack bug reports go through this MTA)

## Mirror pool

* purpose: provide the HTTP and DNS mirror pools

* documentation:

  - [design documentation](https://tails.net/contribute/design/mirrors)
  - [blueprint](https://gitlab.tails.boum.org/tails/blueprints/-/wikis/HTTP_mirror_pool)
  - [[tails/services/mirrors]]
  - [[testing|tails/services/mirrors/testing]]
  - [[updating|tails/services/mirrors/updating]]

* access: public

* tools: [mirrorbits](https://tracker.debian.org/pkg/mirrorbits)

* configuration:

  - [`tails::profile::mirrorbits`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/mirrorbits.pp)

* importance: critical (needed by users to download Tails)

* responsibilities:

  - Process offers of new mirrors.

  - Identify and process broken and slow mirrors.

  - Identify general health problems.

## rsync

* purpose: provide content to the public rsync server, from which all
  HTTP mirrors in turn pull

* access: read-only for those who need it, read-write for Tails core
  developers

* tools: [rsync](https://tracker.debian.org/pkg/rsync)

* configuration:

  - [`tails::profile::rsync`](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/rsync.pp)

  - users and credentials are managed with the `tails_secrets_rsync`
    Puppet module

* importance: critical (needed to release Tails)

## Schleuder

* purpose: host some of our Schleuder mailing lists

* access: anyone can send email to these lists

* tools: [schleuder](https://tracker.debian.org/pkg/schleuder)

* configuration:

  - [`tails::profile::schleuder` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/schleuder.pp)

  - `tails::profile::schleuder::lists` Hiera setting

* importance: high (at least because WhisperBack bug reports go through this service)

## VPN

* purpose: flow through VPN traffic the connections between our
  different remote systems. Mainly used by the monitoring service.

* documentation: [[tails/services/vpn]]

* access: private network.

* tools: [tinc](https://tracker.debian.org/pkg/tinc)

* configuration:

  - [`tails::profile::vpn::instance` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/vpn/instance.pp)

* importance: transitively critical (as a dependency of our monitoring system)

## Web server

* purpose: serve web content for any other service that need it

* access: depending on the service

* tools: [nginx](https://tracker.debian.org/pkg/nginx)

* configuration:

  - [`tails::profile::nginx` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/nginx.pp)

* importance: transitively critical (as a dependency of Jenkins
  and APT repositories)

## Weblate

* URL: <https://translate.tails.net/>

* purpose: web interface for translators

* documentation:

  - [design documentation](https://tails.net/contribute/design/translation_platform)
  - [[modsec|tails/services/weblate]]

* [usage documentation](https://tails.net/contribute/how/translate/with_translation_platform)

* admins: to be defined ([#17050](https://gitlab.tails.boum.org/tails/tails/-/issues/17050))

* tools: [Weblate](https://weblate.org/)

* configuration:

  - [`tails::profile::weblate` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/weblate.pp)

* importance: to be defined

## WhisperBack relay

* purpose: forward bug reports sent with WhisperBack to <tails-bugs@boum.org>

* access: public; WhisperBack (and hence, any bug reporter) uses it

* tools: [Postfix](https://tracker.debian.org/pkg/postfix)

* configuration:

  - [`tails::profile::whisperback` class](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/whisperback.pp)

* importance: high

## Other pages

- [[backups|tails/services/backups]]
- [[puppet-server|tails/services/puppet-server]]
- [[website-builds-and-deployments|tails/services/website-builds-and-deployments]]
- [[website-redundancy|tails/services/website-redundancy]]
- [[xmpp|tails/services/xmpp]]
