# Mirror pool

[[_TOC_]]

First, make sure you read
<https://tails.net/contribute/design/mirrors/>.

We have 2 pools of mirror: a server-side HTTP redirector and a DNS round-robin.
We also maintain a legacy JSON file for compatibility with older Tails versions
and bits of the RM process.

See the "Updating" page below for instructions about how to update
both pools.

## HTTP redirector

- Maintained using Mirrorbits and configured via Puppet.

## DNS

The entries in the DNS pool are maintained directly via PowerDNS using the
`dl.amnesia.boum.org DNS record`. Check the "Updating" page to see how to
change that.

## Legacy JSON file

- Managed by Puppet (tails::profile::mirrors_json).

- Served from: https://tails.net/mirrors.json

- Used by:

  * Tails Upgrader (up to Tails 5.8)
  * Bits of the RM process


## Technical background

### Mirror configuration

Mirror admins are requested to configure their mirror using the
instructions on <https://tails.net/contribute/how/mirror/>.

### rsync chain

1. the one who prepares the final ISO image pushes to
   rsync.tails.net (a VM on lizard, managed by the
   `tails::rsync` Puppet class):
   * over SSH
   * files stored in `/srv/rsync/tails/tails`
   * filesystem ACLs are setup to help, but beware of the permissions
     and ownership of files put into there: the `rsync_tails` group
     must have read-only access
2. all mirrors pull from our public rsync server every hour +
   a random time (manimum 40 minutes)

## Other pages

- [[mirrors/updating]]
- [[mirrors/testing]]
