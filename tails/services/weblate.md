# Mod_security on weblate

:warning: This service will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

This document is a work in progress description of our mod_sec configuration that is protecting weblate.


## How to retrieve the rules and how to investigate whether to keep or remove them

To get a list of the rules that were triggered in the last 2 weeks you can, on
translate.lizard, run:

    sudo cat /var/log/apache2/error.log{,.1} > /tmp/errors
    sudo zcat /var/log/apache2/error.log.{2,3,4,5,6,7,8,9,10,11,12,13,14}.gz >> /tmp/errors
    grep '\[id ' /tmp/errors | sed -e 's/^.*\[id //' -e 's/\].*//' | sort | uniq > /tmp/rules

The file /tmp/rules will now contain all the rules you need to investigate.


## Rules

Below is an overview of the mod_sec rules that were triggered when running weblate.

```
rule nr		remove?	investigate?	reason/comment

"200002"	no			only triggered on calls to non-valid uri's
"911100"	no			only triggered by obvious scanners and bots
"913100"	no			we didn't ask for nmap and openvas scans
"913101"	no			only weird python scanners looking for svg images and testing for backup logfiles
"913102"	no			only bots
"913110"	no			only scanners
"913120"	no			only triggered on calls to invalid uri's
"920170"	no			only triggered on calls to invalid uri's
"920180"	no			the only valid uri was '/' and users shouldn't POST stuff there
"920220"	no			only triggered on calls to invalid uri's
"920230"	yes	no		this rule triggers on calls to comments, which appear to be valid weblate traffic
"920270"	no			only malicious traffic
"920271"	no			only malicious traffic
"920300"	no	yes		this block a call to /git/tails/index/info/refs that happens every minute ??
"920320"	no			only malicious traffic
"920340"	no			only bullshit uri's
"920341"	no			only bullshit uri's
"920420"	no			only bullshit uri's
"920440"	no			only scanners
"920450"	no			only scanners
"920500"	no			only scanners
"921120"	no			only bullshit
"921130"	no			JavaScript injection attempt: var elem = document.getelementbyid("pgp_msg");
"921150"	no			480 blocks in < 10min with ARGS_NAMES:<?php exec('cmd.exe /C echo khjnr2ih2j2xjve9rulg',$colm);echo join("\\n",$colm);die();?>
"921151"	no			only malicious bullshit
"921160"	no			only malicious bullshit
"930100"	no			only malicious bullshit
"930110"	no			only malicious bullshit
"930120"	no			only malicious bullshit
"930130"	no			only malicious bullshit
"931130"	no			only bullshit
"932100"	yes	no		causes false positives on /translate/tails/ip_address_leak_with_icedove/fr/
"932105"	no			only bullshit
"932110"	no			only bullshit
"932115"	yes	no		unsure about some calls and we anyway don't need to worry about windows command injection
"932120"	yes	no		again, unsure, but no need to worry about powershell command injection
"932130"	yes	no		multiple false positives
"932150"	yes	no		false positive on /translate/tails/wikisrcsecuritynoscript_disabled_in_tor_browserpo/fr/
"932160"	no			only bullshit
"932170"	no			only bullshit
"932171"	no			only bullshit
"932200"	yes	no		multiple false positives
"933100"	no			only invalid uri's
"933120"	no			only invalid uri's
"933140"	no			only invalid uri's
"933150"	no			only invalid uri's
"933160"	no			only malicious bullshit
"933210"	yes	no		multiple false positives
"941100"	no			only malicious bullshit
"941110"	no			only malicious bullshit
"941120"	yes	yes		false positive on /translate/tails/faq/ru/
"941150"	yes	no		multiple false positives
"941160"	yes	yes		false positive on /translate/tails/wikisrcnewscelebrating_10_yearspo/fr/
"941170"	no			only bullshit
"941180"	no			only bullshit
"941210"	no			only bullshit
"941310"	yes	no		multiple false positives
"941320"	yes	no		multiple false positives
"941340"	yes	no		multiple false positives
"942100"	no			only bullshit
"942110"	no			only bullshit
"942120"	yes	no		multiple false positives
"942130"	yes	no		multiple false positives
"942140"	no			only bullshit
"942150"	yes	no		multiple false positives
"942160"	no			only bullshit
"942170"	no			only bullshit
"942180"	yes	no		multiple false positives
"942190"	no			only bullshit
"942200"	yes	no		multiple false positives
"942210"	yes	no		multiple false positives
"942240"	no			only bullshit
"942260"	yes	no		multiple false positives
"942270"	no			only malicious bullshit
"942280"	no			only bullshit
"942300"	no			only bullshit
"942310"	no			only bullshit
"942330"	no			only bullshit
"942340"	yes	no		multiple false positives
"942350"	no			only bullshit
"942360"	no			only bullshit
"942361"	no			only bullshit
"942370"	yes	no		multiple false positives
"942380"	no			only bullshit
"942400"	no			only bullshit
"942410"	yes	no		multiple false positives
"942430"	yes	no		multiple false positives
"942440"	yes	no		multiple false positives
"942450"	no			only bullshit
"942470"	no			only bullshit
"942480"	no			only bullshit
"942510"	yes	no		multiple false positives
"943120"	no			only bullshit
"944240"	yes	no		unsure, but we don't need to worry about java serialisation
"949110"	yes	yes		multiple false positives, unsure if this rule can work behind our proxy
"950100"	no			if weblate returns 500, we shouldn't show error messages to third parties
"951120"	yes	no		Message says that the response body leaks info about Oracle, but we don't use Oracle.
"951240"	yes	no		multiple false positives
"952100"	no			only bullshit
"953110"	no			only bullshit
"959100"	yes	yes		multiple false positives, unsure if this rule can work behind our proxy (see 949110)
"980130"	yes	yes		multiple false positives, unsure if this rule can work behind our proxy (see 949110)
"980140"	yes	yes		multiple false positives, unsure if this rule can work behind our proxy (see 949110)
```
