# DNS

:warning: This service will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

Zone creation is done manually using pdnsutil.
We control the following zones:

# amnesia.boum.org

We run the authoritative nameserver for this zone.

The Sysadmin Team is responsible for these records:

 - dl.amnesia.boum.org
 - *.dl.amnesia.boum.org

To change DNS records in this zone, on `dns.lizard`:

	pdnsutil edit-zone amnesia.boum.org

# tails.boum.org

We run the authoritative nameserver for this zone.

To change DNS records in this zone, on `dns.lizard`:

	pdnsutil edit-zone tails.boum.org

# tails.net

We run the authoritative nameserver for this zone.

To change DNS records in this zone, on `dns.lizard`:

	pdnsutil edit-zone tails.net

This zone is secured with DNSSEC. In case of trouble, run:

	pdnsutil rectify-zone tails.net
