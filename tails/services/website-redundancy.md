# Website redundancy

:warning: This process will become outdated with tpo/tpa/team#41947 and this
page should then be updated.

Our website is served in more than one place and we use PowerDNS's LUA records
feature[1][] together with the `ifurlextup` LUA function[2][] to only serve the
mirrors that are up in a certain moment.

## Health checks

Periodic health checks are conducted by the `urlupd`[3][] homegrown service: it
queries a set of IPs passed via the `POOL` environment variable and checks
whether they respond to the `tails.net` domain over HTTPS in port 443. State is
maintained and then served over HTTP in localhost's port 8000 in the format
`ifurlextup` understands.

## DNS record

In the zone file, we need something like this:

```
tails.net	150	IN	LUA	A	("ifurlextup({{"
						 "['204.13.164.63']='http://127.0.0.1:8000/204.13.164.63',"
						 "['94.142.244.34']='http://127.0.0.1:8000/94.142.244.34'"
						 "}})")
```

## Outages

Assuming at least one mirror is up, the duration of a website outage from a
user's perspective should last no more than the sum of the period of health
checks and the DNS record TTL. At the time of writing, this amounts to 180
seconds.

## Website statistics

For sake of simplicity, we reuse our previous setup and website statistics are
sent by each mirror to `tails-dev@boum.org` by a script run by cron once a
month[4][]. Individual stats have to be summed to get the total number of boots
and OpenPGP signature downloads.

---

[1]: https://doc.powerdns.com/authoritative/lua-records/
[2]: https://doc.powerdns.com/authoritative/lua-records/functions.html#ifurlextup
[3]: https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/dns/urlupd.pp
