Testing mirrors
===============

[[_TOC_]]

`check-mirrors.rb` script
-------------------------

This script automates the testing of the content offered by all the
mirrors in the poll. The code can be fetched from:

    <git@gitlab-ssh.tails.boum.org:tails/check-mirrors.git>

It is currently run once a day on [misc.lizard] by
<https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/check_mirrors.pp>.

The code used on [misc] is updated to the latest changes twice an hour
automatically by Puppet.

Install the following dependencies:

	ruby
	ruby-nokogiri.

Usage
-----

### By URL, for the JSON pool

Quick check, verifying the availability and structure of the mirror:

    ruby check-mirrors.rb --ip $IP --debug --fast --url-prefix=$URL

For example:

    ruby check-mirrors.rb --ip $IP --debug --fast --url-prefix=https://mirrors.edge.kernel.org/tails/

Extended check, downloading and verifying the images:

    ruby check-mirrors.rb --ip $IP --debug

### By IP, for the DNS pool

Quick check, verifying the availability and structure of the mirror:

    ruby check-mirrors.rb --ip $IP --debug --fast

Extended check, downloading and verifying the images:

    ruby check-mirrors.rb --ip $IP --debug

### Using check-mirrors from Tails

    torsocks ruby check-mirrors.rb ...
