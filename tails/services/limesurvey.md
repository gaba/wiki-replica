## Updating Limesurvey

:warning: This service will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

We get an e-mail whenever there is a security update available. If such an e-mail arrives, take the following steps:

  - log in to survey
  - check the git log in /var/lib/limesurvey/git to see the latest tag:
    git -C /var/lib/limesurvey/git/ describe --tags --abbrev=0
  - backup the mariadb:
    sudo mysqldump limesurvey | gzip > $(date -I)_survey.tails.net.sql.gz
  - su -s /bin/bash www-data
  - cd /var/www/limesurvey
  - git fetch --all
  - git checkout <TAG>
  - php application/commands/console.php updatedb
  - check https://survey.tails.net if everything still works
