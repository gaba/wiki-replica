# VPN

:warning: This service will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

We're using a VPN between our different machines to interconnect them
and have them managed by lizard's puppetmaster.

:warning: this documentation does not take into account the changes
done in puppet-tails commit da321073230f1feb3076d4296d6ab73f70cbca4f
and friends.

Installation
============

Once you installed the system on a new machine, you'll need to setup the
VPN by hand on it, right before you can go on with the puppet client
setup and first run.

1. On the new system

       apt-get install tinc

2. Create the basic configuration for the Tails VPN:

       export VPN_NAME=tailsvpn
       export VPN_HOSTNAME=$(hostname)
       mkdir -p /etc/tinc/$VPN_NAME/hosts
       echo "Name=$VPN_HOSTNAME" > /etc/tinc/$VPN_NAME/tinc.conf
       echo "Device=/dev/net/tun" >> /etc/tinc/$VPN_NAME/tinc.conf
       echo "Mode=switch" >> /etc/tinc/$VPN_NAME/tinc.conf
       echo "ConnectTo=lizard" >> /etc/tinc/$VPN_NAME/tinc.conf

3. Generate the SSL key pair for this host:

       tincd -n $VPN_NAME -K4096

   … and save the pubkey to `/etc/tinc/$VPN_NAME/rsa_key.pub`

4. Import the other VPN peers hosts file into
   `/etc/tinc/$VPN_NAME/hosts/`. You'll find them on any other VPN
   peers.

5. Create this new host configuration file
   (`/etc/tinc/$VPN_NAME/hosts/$VPN_HOSTNAME`). Use another one as
   example. You just need to change the `Address` field, the `Subnet`
   one, and put the right RSA public key.

6. Mark the VPN as autostarting:

       echo "$VPN_NAME" >> /etc/tinc/nets.boot
       systemctl enable tinc@tailsvpn.service

7. Copy this new host configuration file in the same place on lizard.
   Restart its `tinc@tailsvpn` service.

8. In the node definition in our puppet manifest (`manifests/nodes.pp`),
   add a tails::vpn::instance declaration. Copy the host public key in our
   puppet-tails module
   (`files/vpn/$VPN_NAME/public_keys/$VPN_HOSTNAME.crt`). Add, commit
   and push.

9. Start the VPN on the new host

       systemctl tinc@tailsvpn start

10. Configure the puppet client.

Now you should be able to run the puppet client over the VPN.
