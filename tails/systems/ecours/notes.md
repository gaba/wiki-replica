# Information

* `ecours` is a VM hosted at tachanka:
  <tachanka-collective@lists.tachanka.org>
* We can pay with BitCoin, see `hosting/tachanka/btc-address` in Tor's password store.

# SSH

## SSHd

Hostname: ecours.tails.net

Host key:

    RSA: 9e:0d:1b:c2:d5:68:71:70:2f:49:63:79:43:50:8a:ef
    ed25519: 7f:90:ca:e1:d2:7c:32:54:3e:53:09:36:e8:54:43:6b

## Serial console

Add to your ~/.ssh/config:

    Host ecours-oob.tails.net
        HostName anna.tachanka.org
        User ecours
        RequestTTY yes

Now you should be able to connect to the `ecours` serial console:

    $ ssh ecours-oob.tails.net

The serial console server's host key is:

    RSA SHA256:mleUUuQnVnGI3wIJpWDc+z1JQDS/O/ibVSwirUFS4Eg
    ED25519 SHA256:aGxMMuxg8Nty8OgzJKnWSLwH7fmCJ+caqC+o1tRX1WM

# Network

The kvm-manager instance managing the VMs on the host do not provide
DHCP. We need to use static IP configuration:

FQDN: ecours.tails.net
IP: 209.51.169.91
Netmask: 255.255.255.240
Gateway: 209.51.169.81

## Nameservers

DNS servers reachable from ecours:

209.51.171.179
216.66.15.28
216.66.15.23

# Install

It's running Debian Stretch.

The 20GB virtual disk, partitioned our usual way:

 * /boot 255MB of ext2
 * encrypted volume `vda2_crypt`
  * VG called `vg1`
   * 5GB rootfs LV called `root`
    * ext4 / with relatime and xattr attribute labeled `root`
