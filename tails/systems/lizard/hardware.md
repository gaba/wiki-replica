Parts details
=============

## v2

Bought by Riseup Networks at InterPRO, on 2014-12-12:

* motherboard: Supermicro X10DRi
* CPU: 2 * Intel E5-2650L v3 (1.8GHz, 12 cores, 30M, 65W)
* heatsink: 2 * Supermicro 1U Passive HS/LGA2011
* RAM: 16 * 8GB DDR4-2133MHz, ECC/Reg, 288-pin
  … later upgraded (#11010) to 16 * 16GB DDR4-2133MHz, ECC/Reg,
  288-pin (Samsung M393A2G40DB0-CPB   16GB 2Rx4 PC4-2133P-RA0-10-DC0)
* hard drives (we can hot-swap them!):
  - 2 * Samsung SSD 850 EVO 500GB
  - 2 * Samsung SSD 850 EVO 2TB
  - 2 * Samsung SSD 860 EVO 4TB (slots 1 and 2)
* case: Supermicro 1U RM 113TQ 600W, 8x HS 2.5" SAS/SATA
* riser card: Supermicro RSC-RR1U-E8

Power consumption
=================

(this was before we upgraded RAM and added SSDs)

* 1.23A Peak
* 0.98A Idle

IPMI
====

Your system has an IPMI management processor that allows you to access
it remotely. There is a virtual serial port (which is ttyS1 on the
system) and ability to control power, both of which can be accessed
via command line tools. There is a web interface from which you can
access serial, power, and also if you have java installed you can
access the VGA console and some other features.

Your IPMI network connection is directly connected to a Riseup machine
that has the tools to access it and has an account for you with the ssh
key you provided. The commands you can run from this SSH account are
limited. If you want to get a console, run:

	ssh -p 4422 -t tails@magpie.riseup.net console

To disconnect, use `&.`

To access the power menu:

	ssh -p 4422 -t tails@magpie.riseup.net power

To disconnect, type `quit`

The RSA SSH host key fingerprint for this system is:

	3e:0f:86:51:ce:de:69:db:e1:41:0f:2b:6b:95:29:2b (rsa)
	0f:d4:71:2f:82:6f:0d:37:4d:a6:5c:f5:ed:e1:f8:d3 (ed25519)

More instructions and shell aliases can be found at

  https://we.riseup.net/riseup+colo/ipmi-jumphost-user-docs

Instructions on how to use IPMI are available at

  https://we.riseup.net/riseup+tech/using-ipmi

BIOS
====

Press `<DEL>` to enter BIOS.
