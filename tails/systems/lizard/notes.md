Setup
=====

Network
-------

 * IPv4: 198.252.153.59/24, gateway on .1
 * SeaCCP nameservers:
   - 204.13.164.2
   - 204.13.164.3

Puppet
------

We use dynamic environments matching Git branches:
<https://puppetlabs.com/blog/git-workflow-and-puppet-environments/>

Most nodes are in the `production` environment.

To put a node called `$NODE` in another environment than `production`:

1. Create a topic branch, forked off the `production` branch.
   The name of this topic branch must match /[a-z0-9]+/
   Let's say this branch is called `$TOPIC`.
2. Push that topic branch.
3. On the `production` branch, update our basic external node classifier (ENC)
   so it echoes `environment: $TOPIC` when passed `$NODE` as an argument:
   `modules/site_tails/files/puppet/enc.py`.
4. Push your updated `production` branch.
5. Deploy the updated `/usr/local/bin/puppet-enc` script on our Puppet server:

         ssh puppet.lizard sudo puppet agent --test

Services
========

Gitolite
--------

Gitolite runs on the `puppet-git` VM. It hosts our Puppet modules.

The Puppet manifests and modules are managed in the `puppet-code` Git
repository with submodules. See contribute/git on our website for details.

We use [puppet-sync](https://github.com/pdxcat/puppet-sync) to deploy
the configuration after pushing to Git:

 - `manifests/nodes.pp` (look for `puppet-sync`)
 - `modules/site_puppet/files/git/post-receive`
 - `modules/site_puppet/files/master/puppet-sync`
 - `modules/site_puppet/files/master/puppet-sync-deploy`

dropbear
--------

SSH server, run only at initramfs time, used to enter the
FDE passphrase.

DSS Fingerprint: md5 a3:2e:f8:b6:dd:0a:d1:a6:a8:90:3a:10:18:b7:82:4c
RSA Fingerprint: md5 b4:83:59:1c:6c:12:da:10:d1:2a:a6:0b:8f:e1:49:9a

Services
========

SSH
---

1024 SHA256:tBJk1VUVZZvURMAftdNrZYc4D5RxLuTpu8M+L1jWzB4 root@lizard (DSA)
256 SHA256:E+EH+PkvOCxnVbO8rzDnxJwmO4rqINC3BNnfKPKNwpw root@lizard (ED25519)
2048 SHA256:DeEE4LLIknraA8GZbqMYDZL0CiBjCHWFtOeOhpai89w root@lizard (RSA)

HTTP
----

A HTTP server is running on www.lizard, and receives all HTTP requests
sent to lizard. It plays the role of a reverse-proxy, that is it
forwards requests to the web server that is actually able to answer
the request (e.g. the web server on apt.lizard).

Automatically built ISO images
------------------------------

http://nightly.tails.net/

Virtualization
==============

lizard runs libvirt.

Information about the guest VMs (hidden service name, SSHd
fingerprint) lives in the internal Git repo, as non-sysadmins need
it too.

