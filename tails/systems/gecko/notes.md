# Information

* `gecko` is a VM hosted at tachanka:
  <tachanka-collective@lists.tachanka.org>
* We can pay with BitCoin, see `hosting/tachanka/btc-address` in Tor's password store.
* Internally, they call it `head`.

# SSH

## SSHd

Hostname: gecko.tails.net

Host key:

    256 SHA256:wTKsZrgeTZRS0RERgQJJsvQ2pp5g8HeuUsUyHaw0Bqc root@gecko (ECDSA)
    256 SHA256:FtFHoGGTw7RUk8uhQTgt/JxYBmuC1EspPzFxmAT+WrI root@gecko (ED25519)
    3072 SHA256:HD72IiZYhbRmQI3X3ft4WztLmhNE+Gub+vN8JTVGVZU root@gecko (RSA)

## Serial console

Add to your ~/.ssh/config:

    Host gecko-oob.tails.net
        HostName ursula.tachanka.org
        User head
        RequestTTY yes

Now you should be able to connect to the `head` serial console:

    $ ssh gecko-oob.tails.net

The serial console server's host key is:

    ursula.tachanka.org ED25519 SHA256:9XglwKf0gPHffnhKlgDRLWTB6EuMBAaplBKxhK86JPE
    ursula.tachanka.org RSA SHA256:w7P41LnClVfHf9Te2y3fDkc8YhDO5nSmfdYLtPrIfFs
    ursula.tachanka.org ECDSA SHA256:rSBy7PUW9liNDBl/zjx52DG3nq+a3i4TsiiE5gAnfuE

# Network

The kvm-manager instance managing the VMs on the host do not provide
DHCP. We need to use static IP configuration:

FQDN: gecko.tails.net
IP: 198.167.222.157
Netmask: 255.255.255.0
Gateway:  198.167.222.1
