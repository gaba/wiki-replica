# Hardware

- 1U Asrock X470D4U AMD Ryzen Server short depth
- AMD 3900X 3.8ghz 12-core
- RAM 128GB DDR4 2666 ECC
- NVMe: 2TB Sabrent Rocket 4.0
- NVMe: 2TB Samsung 970 EVO Plus

# Access

Network configuration
---------------------

  - IP:  204.13.164.64
  - Gateway: 204.13.164.1
  - Netmask: 255.255.255.0
  - DNS 1: 204.13.164.4

LUKS prompt
-----------

- The Linux Kernel is unable to show the LUKS prompt in multiple outputs.
- Dragon is currently configured to show the LUKS prompt in its "console",
  which is accessible through the HTTPS web interface (see below), under
  "Remote Control" -> "Launch KVM".
- The reason for choosing console instead of serial for now is that only
  one serial connection is allowed and sometimes we lose access to the BMC
  through the serial console, and then need to access it through HTTPS anyway.

IPMI Access
-----------

IPMI access is made through Riseup's jumphost[1] using binaries from
`freeipmi-tools`[2].

[1] https://we.riseup.net/riseup+colo/ipmi-jumphost-user-docs
[2] https://we.riseup.net/riseup+tech/ipmi-jumphost#jump-host-software-configuration

To access IPMI power menu:

  make ipmi-power

To access IPMI console through the SoL interface:

  make ipmi-console

To access IPMI through the web interface:

  make ipmi-https

TLS Certificate of IPMI web interface
-------------------------------------

The certificate stored in `ipmi-https-cert.pem` is the one found when I fist
used the IPMI HTTPS interface (see the `Makefile` for more). We can eventually
replace it for our own certificate if we want.

SSH Fingerprints
----------------

To see fingerprints for the SSH server installed in the machine:

  make ssh-fingerprints

# Services

Jenkins
-------

Jobs configuration lives in the jenkins-jobs repository:

  - public mirror: https://gitlab-ssh.tails.boum.org:tails/jenkins-jobs
  - production repository: git@gitlab-ssh.tails.boum.org:tails/jenkins-jobs.git

Then see README.mdwn in the jenkins-jobs repository.
