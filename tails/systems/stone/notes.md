# Information

* `stone` is a physical machine hosted at ColoClue in Amsterdam
* It's where our backups are stored.
* ColoClue is a friendly, but not radical, network association that facilitates colocation and runs the AS: https://coloclue.net/en/
* The easiest contact is #coloclue on IRCnet (channel key: cluecolo)
* The physical datacenter is DCG: https://www.thedatacentergroup.nl/

# Special notes

Since we don't want a compromise of lizard to be able to escalate to a compromise of our backups, stone must never become puppetised in our standard way, but always only in a masterless setup!

# SSH

## SSHd

Hostname: stone.tails.net
IP:       94.142.244.35
Onion:    slglcyvzp2h6bgj5.onion

Host key:

    SHA256:p+TQ9IvEGqUMJ5twgb1UweOp6omH4/O1hjwdn4jVk6A root@stone (ED25519)
    SHA256:K+V5AbCrVqWq9Sc1gP28mdXk37umWpFn1v/pYjxZie8 root@stone (ECDSA)
    SHA256:H/Tw12mi2sVTy/dRhlxy6MTQD2xdI76PyG1RweKz9eM root@stone (RSA)


# Rebooting

Dropbear listens on port 443, so:

    ssh -p443 root@stone.tails.net

Host keys:

    SHA256:t1yihiERodaFoW3aebWlXM/FxGTMllf5bqVgSFcjuRw (ECDSA)
    SHA256:gUTcTz4cZhRlK/FiTEUnx+KQWsmzH7sFdAyfl0f8F40 (RSA)
    SHA256:7dkq21tFlT8lWFuTXKSDe5Hl+XzWTCmsBOGQRSyptcU (DSS)

Once logged in, simply type:

    cryptroot-unlock

If the machine is not up and running within a minute or two, connect to the serial console to have a look what's going on.


# OOB

Out of band access goes through ColoClue's console server, which allows for remote power on/off and serial access:

    ssh groente@service.coloclue.net

Host key:

    SHA256:31K4uqPcMa91wy30pk3PJKfe865OZMrGDrfVXjiU0Ds (RSA)


# Installation

Base debian install with RAID5, LVM and FDE

apt-get install linux-image-amd64 dropbear puppet git-core shorewall

configured dropbear manually

set up masterless puppet, all further changes are in there
