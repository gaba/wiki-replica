OOB access
----------

## Connect to the VPN

Chameleon doesn't have a serial console, but rather an IPMI interface which is
available via ColoClue's VPN.

To connect to the VPN:

```
make vpn
```

Note: you may need root for that.

Username is "groente" and the password is the ColoClue password stored in our
password store:

```
pass tor/hosting/coloclue.net
```

## Access the IPMI web interface

That command should show you the TLS certificate fingerprints to check when you
access the IPMI interface via:

- https://94.142.243.34

Note: connect to the IPMI web interface using the IP, otherwise the browser may
not allow you to proceed because of the self-signed certificate.

The IPMI username and password are also in the password-store:

```
pass tor/oob/chameleon.tails.net/ipmi 
```

## Launch the remote console

To access the console, choose "Remote Control" -> "iKVM/HTML5"
