Skink
=====

It is a bare metal machine for dev/test purposes provided by PauLLA
(https://paulla.asso.fr) free of charge.

Contact: noc@lists.paulla.asso.fr

Machine
-------

- Intel(R) Xeon(R) CPU E5520 @ 2.27GHz
- 24 GB RAM
- 2x 512 GB SSD

Debian installation was made using https://netboot.xyz, with disks mirrored
using software RAID 1 and then LUKS + LVM.

Network
-------

IPv4:

- The subnet 45.67.82.168/29 is assigned to us
- The IPs from 45.67.82.169 to 45.67.82.171 are reserved for the PauLLA
  routers.
- We can use the IPs from 45.67.82.172 to 45.67.82.174 with 45.67.82.169 for
  gateway.

IPv6:

- The subnet 2a10:c704:8005::/48 is assigned to us.
- 2a10:c704:8005::/64 is provisioned for interconnection.
- IPs from 2a10:c704:8005::1 to 2a10:c704:8005::3 are reserved for PauLLA
  routers.
- We can use IPs from 2a10:c704:8005::4 with 2a10:c704:8005::1 for gateway.
- The rest of the /48 can be routed anywhere you want.

OOB access
----------

- Hostname: telem.paulla.asso.fr
- Port: 22
- Account: tails
- SSH fingerprints: See `./known_hosts/telem.paulla.asso.fr/ssh`
- IPMI password: `pass tor/oob/skink.tails.net/ipmi`
- Example IPMI usage, see: `ipmi.txt`

See `Makefile` for example OOB commands.

Dropbear access
---------------

See fingerprints in `./known_hosts/skink.tails.net/dropbear`.

See `Makefile` for example Dropbear commands.

SSH access
----------

See fingerprints in `./known_hosts/skink.tails.net/ssh`.

See `Makefile` for example SSH commands.
